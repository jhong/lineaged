"use strict";
const treeState = require('./treeState');
const cell3dState = require('./cell3dState');
const interfaceState = require('./interfaceState');
const datasetState = require('./datasetState');
const studyState = require('./studyState');
const utils = require('./utils');
const cell3dModel = require('./cell3dModel');
const cell3dView = require('./cell3dView');
const treeModel = require('./treeModel');
const treeView = require('./treeView');

let startingTime;
let clicked_times = 0;
let last_clicking_timestamp = null;

// Change colors in both cell3d and tree
function changeColor(colorMode) {
    let tempColors = [];
    if (colorMode == "district") {
        cell3dState.colors = cell3dState.colors_district;
        interfaceState.currentColorMode = colorMode;
    }
    else if (colorMode == "area") {
        cell3dState.colors = cell3dState.colors_area;
        interfaceState.currentColorMode = colorMode;
        tempColors = ['#4C004C', '#FF00FF'];
    }
    else if (colorMode == "random") {
        cell3dState.colors = cell3dState.colors_random;
        interfaceState.currentColorMode = colorMode;
    }
    else if (colorMode == "confidence") {
        cell3dState.colors = cell3dState.colors_confidence;
        interfaceState.currentColorMode = colorMode;
        tempColors = ['#007F7F', '#00FFFF'];
    }
    cell3dView.changeColor(colorMode);
    treeView.changeColors(colorMode, tempColors);
    if (cell3dState.highlighted!= null) {highlightCell(cell3dState.highlighted);}
}

// highlight cell in 3d and in tree
function highlightCell(d) {
    treeView.highlightCellinTree(d);
    if (d.name == undefined) {
        cell3dView.highlightCell3d(d)
    } else {
        cell3dView.highlightCell3d(d.name)
    }
}


function freshTree() {
    treeView.clear();
    treeView.clearTopTree();
    treeModel.updateFromOrder(cell3dState.cellOrder);
    treeView.update(cell3dState.cellOrder, cell3dView.switchToLevel, highlightCell, changeColor, doubleClickReaction);
    treeState.slider.value(cell3dState.currentLevel);
    cell3dModel.getAreaColor();
    changeColor(interfaceState.currentColorMode);
}

function processClickDivisionSelection(event) {
    const {parent, worldPosition} = cell3dView.getProcessedSelection(event);
    if (parent == -1) {return;}
    if (!treeState.clickDivideCells.includes(parent)){
        treeState.clickDivideCells.push(parent);
        let index = cell3dState.existingCells.indexOf(parent);
        cell3dState.actors[index].getProperty().setColor([0,1,0]);
    }
    cell3dView.updateWorldPosition(worldPosition);
}

function processMainViewSelections(event) {
    const {parent, worldPosition} = cell3dView.getProcessedSelection(event);
    if (parent == -1) {return;}
    treeView.moveTree(parent);
    highlightCell(parent);
    // Update picture for the user so we can see the green one
    cell3dView.updateWorldPosition(worldPosition);
}

function changeToMainView() {
    cell3dView.changeToMainView();
    clicked_times = 0;
}

// double click in the main view will switch to neighbors view
function changeToNeighborsView(name) {
    cell3dView.changeToNeighborsView(name);
    clicked_times = 0;
}

function pickOnMouseDownEvent() {
    startingTime = performance.now();
}


function pickOnMouseEventOverview(event) {
    if (cell3dView.isAnimating()) {
        // we should not do picking when interacting with the scene
        return;
    }
    if (treeState.clickDivideStatus == true) {
        processClickDivisionSelection(event);
        return;
    }
    if ((performance.now()-startingTime) > 300) {
        // this is used to detect if rotation to not change the cell picked
        return;
    }
    if (cell3dState.currentLevel>treeState.levelNum.length) {return;}

    clicked_times++;
        
    // single click
    if (performance.now()-last_clicking_timestamp > 300){
        clicked_times = 1;
        studyState.actionRow.push(["single click", performance.now()-studyState.startingTime]);
        last_clicking_timestamp = performance.now();
        // with all cells
        if (cell3dState.currentMode == "mainView") {
            processMainViewSelections(event);
        }
        // with only neighbors
        if (cell3dState.currentMode == "neighboringView") {
            cell3dView.processNeighboringSelections(event);
        }
    }
    // double click - show all the neighbors in the view
    if (clicked_times == 2) {
        studyState.actionRow.push(["double click", performance.now()-studyState.startingTime]);
        doubleClickReaction();
    }
}

function doubleClickReaction() {
    if (cell3dState.currentMode == "mainView" && cell3dState.highlighted != null) {
        cell3dView.deleteAllOtherCells(cell3dState.highlighted);
        changeToNeighborsView(cell3dState.highlighted);
    }
    else if (cell3dState.currentMode == "neighboringView") {
        changeToMainView();
    }
}


function mergeTwoTrees() {
    let highestDepthOfLasso = 0,
        lowestDepthOfLasso = 100;
    for (let lasso of treeState.lassoParent) {
        if (Math.max(...lasso.depth) > highestDepthOfLasso) {highestDepthOfLasso = Math.max(...lasso.depth);}
        if (Math.min(...lasso.depth) < lowestDepthOfLasso) {lowestDepthOfLasso = Math.min(...lasso.depth);}
    }
    for (let i = lowestDepthOfLasso; i <= highestDepthOfLasso; i++) {
        let cells = getNoParentCell();
        for (let lasso of treeState.lassoParent) {
            if (lasso.depth.includes(i)) {
                for (let cell of cells) {
                    if (utils.arraysEqual(cell.leaves,lasso.leaves)) {break;}
                    if (utils.isSuperset(lasso.leaves, cell.leaves)) {
                        const difference = utils.sortArrayDescending(utils.differenceOfSets(lasso.leaves, cell.leaves));
                        for (let cell2 of cells) {
                            if (utils.arraysEqual(cell2.leaves,difference)) {
                                cell3dState.assignment.push(0);
                                let toCreate = cell3dState.existingCells[cell3dState.existingCells.length-1] +1;
                                cell3dState.existingCells.push(toCreate);
                                cell3dState.assignment[cell3dState.existingCells.indexOf(cell.name)] = toCreate;
                                cell3dState.assignment[cell3dState.existingCells.indexOf(cell2.name)] = toCreate;
                                treeModel.recordTreeBackgroundColor([cell.name, cell2.name], "blue");
                                treeModel.editPossibilities(cell.name, cell2.name, 0, 0);
                                cell3dModel.freshData();
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
}

function getNoParentCell() {
    let cells = [];
    for (let i = 0; i < cell3dState.assignment.length; i++) {
        if (cell3dState.assignment[i] == 0) {
            cells.push({});
            cells[cells.length-1]["name"] = cell3dState.existingCells[i];
            let leaves = cell3dModel.findAllLeaves(cell3dState.existingCells[i]);
            cells[cells.length-1]["leaves"] = utils.sortArrayDescending(leaves);
        }
    }
    return cells;
}

module.exports = {
    changeColor,
    highlightCell,
    freshTree,
    pickOnMouseEventOverview,
    pickOnMouseDownEvent,
    changeToMainView,
    mergeTwoTrees,
    doubleClickReaction
}