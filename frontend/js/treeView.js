
const interfaceState = require('./interfaceState');
const cell3dState = require('./cell3dState');
const treeState = require('./treeState');
const treeModel = require('./treeModel');
const utils = require('./utils');
const studyState = require('./studyState');

function createInstructions() {
  d3.select('#instructions')
  .append('svg')
  .attr('width', interfaceState.svgWidth)
  .attr('height', 48);
}

function createTopButtons(callback) {
  // click the default radio button
  var radiobtn = document.forms["colorForm"].elements['choice'];
  for (var i = 0; i < radiobtn.length; i++) {
      radiobtn[i].onclick = function(){
          callback(this.value);
      };
  }
}

function addTreeBackgroundColor(name, color, toAddOrRecreate) {
  let sister = null;
  if (toAddOrRecreate == true && cell3dState.neighboringTried != null) {sister = cell3dState.neighboringTried;}
  else {
      let index = cell3dState.cellOrder.indexOf(name);
      let parent = cell3dState.assignedParent[index];
      if (parent != 0){
          sister = cell3dState.children[cell3dState.cellOrder.indexOf(parent)].slice();
          sister.splice(sister.indexOf(name),1);
      }
  }
  if (sister == null) {console.log("error happened when trying to find the sister;"); return;}
  // get two points of two cell
  sister = sister[0];
  const index1 = cell3dState.cellOrder.indexOf(name),
        index2 = cell3dState.cellOrder.indexOf(sister);
  let x1 = treeState.nodesXs[index1],
      x2 = treeState.nodesXs[index2],
      y1 = treeModel.nodeY(index1);
  let width = treeState.nodeWidths[index1] + treeState.nodeWidths[index2];
  const x = Math.min(x1, x2) - treeState.focusMoved/treeState.diff,
        y = y1 - interfaceState.nodeVerticalMargin + interfaceState.nodeHorizontalMargin;
  // Create Pattern
  treeState.background
      .append("rect")
      .attr("class", "lines")        
      .attr("x", x)
      .attr("y", y)
      .attr("width", width + interfaceState.nodeHorizontalMargin)
      .attr("height", interfaceState.nodeVerticalMargin - 2 * interfaceState.nodeHorizontalMargin)
      .attr("fill", color);
}

function addTopTreeBarColor() {
    for (let lassoParent of treeState.lassoParent) {
        if (lassoParent.children.length > 0) {
            const index = treeState.lassoParent.indexOf(lassoParent);
            let height = interfaceState.nodeVerticalMargin - 2 * interfaceState.nodeHorizontalMargin;
            let x = treeModel.lassoX(index);
            let y = treeModel.lassoY(index) + treeModel.lassoHeight(index)+ (interfaceState.nodeVerticalMargin-height)/2;
            let width = treeModel.lassoWidth(index);
            d3.select("svg#topTree")
                .append("rect")
                .attr("class", "topBars")
                .attr("x", x)
                .attr("y", y)
                .attr("width", width)
                .attr("height", height)
                .attr("fill", "blue");
        }
    }
}

function confirmAllCurrentPairs() {
    if (treeState.levelNum.length >= 2) {
        for (let cell of cell3dState.cellOrder) {
            const index = cell3dState.cellOrder.indexOf(cell),
                parent = cell3dState.assignedParent[index];
            if (parent != 0) {
                if (utils.checkIteminObjectArray(cell, treeState.backgroundCorrespondingChildren, "name") != -1) {continue;}
                addTreeBackgroundColor(cell, "green", true);
                let sister = cell3dState.children[cell3dState.cellOrder.indexOf(parent)].slice();
                sister.splice(sister.indexOf(cell),1);
                treeModel.recordTreeBackgroundColor([cell, sister[0]], "green");
            }
        }
    }
}


function clear() {
  d3.select("svg#mainsvg").selectAll("*").remove();
  d3.select("div#thumb_container").selectAll("*").remove();
  d3.select("#target_neighbors").select("svg").remove();
  d3.select("div#slider_level").selectAll("*").remove();
}

function clearTopTree() {
    d3.select("svg#topTree").selectAll("*").remove();
    d3.select("svg#topTree").attr("height",0);
    d3.select("svg#dots").selectAll("*").remove();
    d3.select("svg#dots").attr("height",0);
}

function createSlider(callback){
  let maxValue = treeState.levelNum.length;
  if (treeState.lassoParent.length >= 1) {maxValue = Math.max(treeState.lassoParent[0].depth);}
  const mainTreeMax = treeState.levelNum.length;  
  let slider_height = (interfaceState.nodeDefaultHeight-6)*maxValue,
      slider_width = 70,
      half_node = (interfaceState.svgHeight/mainTreeMax-interfaceState.nodeVerticalMargin)/2,
      slider_margin = {top: interfaceState.nodeDefaultHeight/2, 
        bottom: half_node};

  let values = [];
  for (let i = 1; i <= maxValue; i++) {
      if (i <= mainTreeMax) {
          values.push(i);
      } else if (i == mainTreeMax+1) {
          values.push("...");
      } else if (i == maxValue){
          values.push("N");
      } else {
          values.push("N-"+(maxValue-i));
      }
  }

  treeState.slider = d3.sliderVertical()
      .min(1)
      .max(maxValue)
      .step(1.0)
      .width(slider_width)
      .height(slider_height)
      .tickFormat(function(d,i){return values[i]})
      .ticks(maxValue-1)
      .displayValue(false)
      .handle(
        d3.symbol()
          .type(d3.symbolCircle)
          .size(200)
      )
      .on('onchange', val => {
            callback(val);
            let focusView = null;
            if (treeState.workingTree == "mainTree") {focusView = d3.select("svg#mainsvg").select("g");}
            else {focusView = d3.select("svg#topTree").select("g");}
            if (treeState.marked != null) {
                focusView.selectAll("rect")
                    .filter(function(d,i){
                        if (d.name == undefined) {return d == treeState.marked;}
                        else {return d.name == treeState.marked;}
                    })
                    .style("stroke", "none");
            }
      });

  d3.select("div#slider_level").append('svg')
      .attr('width', slider_width)
      .attr('height', interfaceState.svgHeight)
      .append('g')
      .attr("transform", 'translate(' + slider_width/2 + ',' + slider_margin.top +')')
      .call(treeState.slider);
}

// TODO: module scoped var
let focusView;
function createTree(incomingData, callback, clickingCallback) {
    treeState.background = d3.select("svg#mainsvg");
    var tree = d3.select("svg#mainsvg")
        .attr("width", interfaceState.svgWidth)
        .attr("height", (interfaceState.nodeDefaultHeight+interfaceState.nodeVerticalMargin)*treeState.levelNum.length)
        .attr("data-cy", "tree-mainsvg-g")
        .append("g")
        .selectAll("g")
        .data(incomingData)
        .enter()

    focusView = tree
        .append("g")
        .attr("class", "nodes");
    var treeNodes = d3.selectAll("g.nodes")
                    .attr("class", "node");
    
    treeNodes
        .append("rect")
        .attr("class", "left")
        .attr("width", function(d, i) {return treeModel.nodeWidth(i); })
        .attr("height", function(d, i) {return treeModel.nodeHeight(d);})
        .attr("x", function(d, i) {return treeModel.nodeX(i);})
        .attr("y", function(d,i) {return treeModel.nodeY(i);})
        .attr("fill", function(d, i) {
            if (interfaceState.currentColorMode == "district") {return utils.rgbToHex(treeModel.nodeColor(i));}
            else {callback(interfaceState.currentColorMode); return utils.rgbToHex(cell3dState.colors[cell3dState.existingCells.indexOf(cell3dState.cellOrder[i])]);}
        })
        .attr('pointer-events', 'mouseover')
        .on("mouseover", function(d) { 
            d3.select(this).attr("fill", "rgb(1,1,1,0.2)");
        })
        .on("mouseout", function(d,i){
            if (cell3dState.colors.length < cell3dState.existingCells.length) {
                d3.select(this).attr("fill", utils.rgbToHex(treeModel.nodeColor(i)));
            }
            else {
                d3.select(this).attr("fill", utils.rgbToHex(cell3dState.colors[cell3dState.existingCells.indexOf(cell3dState.cellOrder[i])]));
            }
        });
    
    treeNodes
        .append("rect")
        .attr("width", function(d, i) {return treeModel.nodeWidth(i); })
        .attr("height", function(d, i) {return treeModel.nodeHeight(d);})
        .attr("x", function(d, i) {return treeModel.nodeX(i);})
        .attr("y", function(d,i) {return treeModel.nodeY(i);})
        .attr("fill", 'url(#unchecked)');

    treeNodes
        .append("text")
        .attr("x", function(d, i){return treeModel.nodeX(i)+treeState.nodeWidths[i]/2;})
        .attr("y", function(d,i) {return treeModel.nodeY(i)+interfaceState.nodeDefaultHeight/2;})
        .style("fill", "white")
        .style("stroke", "white")
        .text(function(d) {
            if (d.name == undefined) {return d;}
            else {return d.name;}
        })  
        .on("click", function(d){
            studyState.actionRow.push(["2d click", performance.now()-studyState.startingTime]);
            clickingCallback(d);
        });

    if (treeState.backgroundCorrespondingChildren.length > 0) {
      for (let item of treeState.backgroundCorrespondingChildren) {
        addTreeBackgroundColor(item["name"][0], item["color"], false);
      }
    }
}

function constructTreePicking(callback, doubleClickCallback) {
    d3.select("svg#mainsvg").selectAll("rect")
        .on("click", function(d){
            treeState.workingTree = "mainTree";
            if (cell3dState.currentMode == "neighboringView") {
                doubleClickCallback();
            }
            studyState.actionRow.push(["2d click", performance.now()-studyState.startingTime]);
            callback(d);
        })
        .on("dblclick", function(d){
            treeState.workingTree = "mainTree";
            doubleClickCallback();
        });
}

// lasso select to form the top
function createTopTree() {
    const [depthMin, depthMax] = findMinMaxDepthTopTree();
    let topTree = d3.select("svg#topTree")
        .attr("width", interfaceState.svgWidth)
        .attr("height", (interfaceState.nodeDefaultHeight+interfaceState.nodeVerticalMargin)*(depthMax-depthMin+1))
        .append("g")
        .selectAll("g")
        .data(treeState.lassoParent)
        .enter();

    topTree
        .append("rect")
        .attr("width", function(d, i) {return treeModel.lassoWidth(i);})
        .attr("height", function(d, i) {return treeModel.lassoHeight(i);})
        .attr("x", function(d, i) {return treeModel.lassoX(i);})
        .attr("y", function(d, i) {return treeModel.lassoY(i);})
        .attr("fill", function(d,i) {const color = utils.averageColors(d.leaves, cell3dState.existingCells, cell3dState.colors);
                                    return utils.rgbToHex(color)});

    topTree
        .append("text")
        .attr("x", function(d, i) {return treeModel.lassoX(i)+treeModel.lassoWidth(i)/2})
        .attr('y', function(d, i) {return treeModel.lassoY(i)+interfaceState.nodeDefaultHeight/2})
        .style("fill", "white")
        .style("stroke", "white")
        .text(function(d, i) {return ("Cell " +treeState.lassoParent[i].name)});
    
    addTopTreeBarColor();
}

function findMinMaxDepthTopTree() {
    let min = treeState.lassoParent[0].depth[0];
    let max = treeState.lassoParent[0].depth[0];
    for (let ob of treeState.lassoParent) {
        if (ob.depth[0] > max) {max = ob.depth[0];}
        else if (ob.depth[0] < min) {min = ob.depth[0];}
    }
    return [min, max];
}

// dots are representing emptys there
function createDots(){
    const data = [1,2,3,4,5];
    const r = 5;
    let dots = d3.select("svg#dots")
        .attr("width", 200)
        .attr("height", 50)
        .append("g")
        .selectAll("g")
        .data(eval(data))
        .enter()
        .append("circle")
        .attr("cx", function(d, i){return 4*r*d-2*r})
        .attr("cy", 50/2)
        .attr("r", r)
        .attr("fill", "grey");
}

function constructTopTreePicking(callback) {
    d3.select("svg#topTree").selectAll("rect")
        .on("click", function(d){
            treeState.workingTree = "topTree";
            studyState.actionRow.push(["2d click", performance.now()-studyState.startingTime]);
            callback(d);
        });
}

// TODO: code duplication, to refactor
function highlightCellinTree(d){
    let focusView = null;
    if (treeState.workingTree == "mainTree") {focusView = d3.select("svg#mainsvg").select("g");}
    else {focusView = d3.select("svg#topTree").select("g");}
    let lastFocusView = null;
    if (isNaN(treeState.marked)){lastFocusView = d3.select("svg#topTree").select("g");}
    else {lastFocusView = d3.select("svg#mainsvg").select("g");}
    // remove stroke
    if (treeState.marked != null && treeState.marked != d && treeState.marked != d.name) {
        lastFocusView.selectAll("rect")
            .filter(function(d,i){
                if (d.name == undefined) {return d == treeState.marked;}
                else {return d.name == treeState.marked;}
            })
            .style("stroke", "none");
    }
    if (d.name == undefined && !isNaN(d)) {
        if (!cell3dState.timeDepths[cell3dState.cellOrder.indexOf(d)].includes(cell3dState.currentLevel)) {
            const value = cell3dState.timeDepths[cell3dState.cellOrder.indexOf(d)][0];
            // TODO: bug here, switchToLevel in cell3dView, should we include another view in a view ?
            // or move the logic to a controller ?
            // switchToLevel(value);
            treeState.slider.value(value);
        }
        treeState.marked = d;
        if (treeState.marked == d) { 
            focusView.selectAll("rect")
                .filter(rectName => {return rectName == treeState.marked;})
                .style("stroke", "red")
                .style("stroke-width", "2px");
        }
    }
    else {
        if (treeState.workingTree == "mainTree" &&
            !cell3dState.timeDepths[cell3dState.cellOrder.indexOf(d.name)].includes(cell3dState.currentLevel)) {
            const value = cell3dState.timeDepths[cell3dState.cellOrder.indexOf(d.name)][0];
            treeState.slider.value(value);
            treeState.marked = d.name;
        } else if (treeState.workingTree == "mainTree") {
            treeState.marked = d.name;
            focusView.selectAll("rect")
                .filter(function(d,i){return d.name == treeState.marked;})
                .style("stroke", "red")
                .style("stroke-width", "2px");
        }
        if (treeState.workingTree == "topTree") {
            let object;
            if (d.name == undefined) {
                const index = utils.checkIteminObjectArray(d, treeState.lassoParent, "name");
                object = treeState.lassoParent[index];
            } else { object = d;}
            if (!object.depth.includes(cell3dState.currentLevel)){
                const value = object.depth;
                treeState.slider.value(value);
            }
            treeState.marked = object.name;
        } 
        focusView.selectAll("rect")
            .filter(function(d,i){
                if (d.name == undefined) {return d == treeState.marked;}
                else {return d.name == treeState.marked;}
            })
            .style("stroke", "red")
            .style("stroke-width", "2px");
    }
    if (treeState.workingTree == "mainTree"){
        let index = null;
        if (d.name == undefined) {index = cell3dState.cellOrder.indexOf(d);}
        else {index = cell3dState.cellOrder.indexOf(d.name);}
        let temp = treeState.nodesXs[index] + treeState.nodeWidths[index]/2- treeState.focusMoved/treeState.diff;
        if (temp>interfaceState.svgWidth-treeState.nodeWidths[index]/2) {moveabit(1);}
        if (temp<treeState.nodeWidths[index]/2) {moveabit(-1)};
    }
}

// 1 == right, -1 == left
function moveabit(direction) {
    let focusView = d3.select("svg#mainsvg").select("g");
    var moveDistance = interfaceState.nodeDefaultWidth * direction * treeState.diff;
    var thumb_rectX = Math.max(0, Math.min(interfaceState.svgWidth-50-interfaceState.svgWidth*treeState.diff, treeState.focusMoved+moveDistance));
    treeState.focusMoved = thumb_rectX;
    d3.select("svg.thumbnail_svg").select("rect.range")
        .attr("x", function(){
            return thumb_rectX;
        });
    focusView.selectAll("rect")
        .attr("x", function(d, i){
            if (d.name == undefined) {var index = cell3dState.cellOrder.indexOf(d);}
            else {var index = cell3dState.cellOrder.indexOf(d.name);}
            var temp = treeState.nodesXs[index] - thumb_rectX/treeState.diff;
            return temp;
        });
    focusView.selectAll("text")
        .attr("x", function(d,i) {
            if (d.name == undefined) {var index = cell3dState.cellOrder.indexOf(d);}
            else {var index = cell3dState.cellOrder.indexOf(d.name);}
            var temp = treeState.nodesXs[index] + treeState.nodeWidths[index]/2- thumb_rectX/treeState.diff;
            return temp;
    });
    if (treeState.backgroundCorrespondingChildren.length > 0) {
      treeState.background.selectAll(".lines")
        .attr("x", function(d, i){
            const children = treeState.backgroundCorrespondingChildren[i]["name"];
            const child1 = children[0], 
                child2 = children[1];
            const index1 = cell3dState.cellOrder.indexOf(child1),
                  index2 = cell3dState.cellOrder.indexOf(child2);
            var temp = Math.min(treeState.nodesXs[index1], treeState.nodesXs[index2]) - thumb_rectX/treeState.diff;
            return temp;
        });
    }
}

// TODO: module scoped var
var thumbnails;
function createThumbnail(cellOrder) {
    let incomingData = cellOrder;
    if (treeState.lassoParent.length > 0) {
        incomingData = incomingData.concat(treeState.lassoParent);
    }
    d3.select("div#thumb_container")
        .append("svg")
        .attr("width", interfaceState.svgWidth)
        .attr("height", interfaceState.nodeDefaultHeight/6*1.1*treeState.levelNum.length+50)
        .attr("class", "thumbnail_svg")
        .append("g")
        .selectAll("g")
        .data(incomingData)
        .enter()
        .append("g")
        .attr("class", "thumbnails");

    thumbnails = d3.selectAll("g.thumbnails");
    const totalWidth = treeModel.getTotalWidthOfTree();
    treeState.diff = interfaceState.svgWidth/totalWidth;

    let maxYTopTree = 0;
    if (treeState.lassoParent.length > 0) {
        const depth = Math.max(...treeState.lassoParent[0].depth);
        for (let top of treeState.lassoParent) {
            const tmp = depth - Math.max(...top.depth);
            if (tmp > maxYTopTree) {
                maxYTopTree = tmp;
            }
        }
        maxYTopTree += 1;
    }
    

    thumbnails
        .append("rect")
        .attr("width",function(d,i) {
            if (i < cellOrder.length) {return treeState.nodeWidths[i]*treeState.diff;}
            else {return treeModel.lassoWidth(i-cellOrder.length)*treeState.diff;}
        })
        .attr("height", function(d,i) {
            if (i < cellOrder.length) {return treeModel.nodeHeight(d)/6;}
            else {return treeModel.lassoHeight(i-cellOrder.length)/6;}
        })
        .attr("x", function(d, i) {
            if (i < cellOrder.length) {return treeModel.nodeX(i)*treeState.diff;}
            else {return treeModel.lassoX(i-cellOrder.length)*treeState.diff;}
        })
        .attr("y", function(d, i) {
            if (i < cellOrder.length) {
                if (d.name == undefined) {
                    const y = treeModel.nodeY(cell3dState.cellOrder.indexOf(d));
                    return (y + maxYTopTree*interfaceState.nodeDefaultHeight)/6;}
                else {
                    const y = treeModel.nodeY(cell3dState.cellOrder.indexOf(d.name));
                    return (y + maxYTopTree*interfaceState.nodeDefaultHeight)/6;}
            }
            else {return treeModel.lassoY(i-cellOrder.length)/6;}
        })
        .attr("fill", function(d, i) {
            if (i < cellOrder.length){
                if (interfaceState.currentColorMode == 'district') {return treeState.cellColors[i];}
                else {return utils.rgbToHex(cell3dState.colors[cell3dState.existingCells.indexOf(cell3dState.cellOrder[i])]);}
            } else {
                const color = utils.averageColors(d.leaves, cell3dState.existingCells, cell3dState.colors);
                return utils.rgbToHex(color);
            }
        })
        .attr("opacity", 0.8);

    d3.select("svg.thumbnail_svg")
        .append("rect")
        .attr("width", function(){return interfaceState.svgWidth*treeState.diff;})
        .attr("height", interfaceState.nodeDefaultHeight/6*(treeState.levelNum.length+maxYTopTree))
        .attr("x", 0)
        .attr("class", "range")
        .attr('fill', 'white')
        .attr("opacity", 0.4)
        .style("stroke", "red")
        .style("stroke-width", 3)
        .call(d3.drag()
            .on("drag", thumbnailDragged)
            );
}

function createZoomSlider(freshCallback) {
    const sliderSelect = '#slider_zoomTree';
    const slider_height = 100;
    const slider_width = $(document).width()*0.5;
    const margin = {right:30, left:30};
    const fixedMargin = interfaceState.nodeHorizontalMargin*(cell3dState.existingCells.length-1);
    const zoomSliderMax = 1-(interfaceState.svgWidth-fixedMargin)/(treeModel.getTotalWidthOfTree()-fixedMargin);

    const slider = d3.sliderHorizontal()
        .min(0)
        .max(zoomSliderMax)
        .width(slider_width*interfaceState.smallWindowWidthRatio)
        .height(slider_height)
        .displayValue(false)
        .ticks(5)
        .on('onchange', val => {
            console.log(val);
            interfaceState.nodeDefaultWidth = 30 * (1-val);
            freshCallback();
        })
    
    const d3SliderSelect = d3.select(sliderSelect);

    d3SliderSelect.append('svg')
        .attr('width', slider_width)
        .attr('height', slider_height)
        .attr('data-cy', 'cell3d-slider-svg')
        .append('g')
        .attr("transform", 'translate(' + margin.left + ',' + margin.right +')')
        .call(slider);

    let element = document.getElementById("slider_zoomTree");
    element.style.marginTop = "-20px";
    element.style.marginBottom = "-20px";
}


function thumbnailDragged() {
    const thumb_rectX = Math.max(0, Math.min(interfaceState.svgWidth-interfaceState.svgWidth*treeState.diff, d3.event.x-interfaceState.svgWidth*treeState.diff/2));
    treeState.focusMoved = thumb_rectX;
    d3.select("svg.thumbnail_svg").select("rect.range")
        .attr("x", function(){
            return thumb_rectX;
        });
    focusView.selectAll("rect")
        .attr("x", function(d, i){
            if (d.name == undefined) {var index = cell3dState.cellOrder.indexOf(d);}
            else {var index = cell3dState.cellOrder.indexOf(d.name);}
            var temp = treeState.nodesXs[index] - thumb_rectX/treeState.diff;
            return temp;
        });
    focusView.selectAll("text")
        .attr("x", function(d,i) {
            if (d.name == undefined) {var index = cell3dState.cellOrder.indexOf(d);}
            else {var index = cell3dState.cellOrder.indexOf(d.name);}
            var temp = treeState.nodesXs[index] + treeState.nodeWidths[index]/2- thumb_rectX/treeState.diff;
            return temp;
        });
    if (treeState.backgroundCorrespondingChildren.length > 0) {
        treeState.background.selectAll(".lines")
            .attr("x", function(d, i){
                const children = treeState.backgroundCorrespondingChildren[i]["name"];
                const child1 = children[0], 
                    child2 = children[1];
                const index1 = cell3dState.cellOrder.indexOf(child1),
                    index2 = cell3dState.cellOrder.indexOf(child2);
                var temp = Math.min(treeState.nodesXs[index1], treeState.nodesXs[index2]) - thumb_rectX/treeState.diff;
                return temp;
            });
    }

    let topTreeFocused = d3.select("svg#topTree").select("g");
    topTreeFocused.selectAll("rect")
        .attr("x", function(d, i) {
            let tmp = treeModel.lassoX(i) - thumb_rectX/treeState.diff;
            return tmp;
        });
    topTreeFocused.selectAll("text")
        .attr("x", function(d, i) {
            let tmp = treeModel.lassoX(i) + treeModel.lassoWidth(i)/2 - thumb_rectX/treeState.diff;
            return tmp;
        })
}

// if flag = false, means to hide. otherwise, show the nodes
function hideOrShowInThumbnail(name, flag) {
  var allChildren = utils.getAllChildren(name);
  thumbnails.selectAll("rect")
      .filter(function(d,i){return allChildren.includes(d.name);})
      .style("visibility", function(){
          return (flag) ? "visible":"hidden";
      });
}

function update(cellOrder, callback, treePickingCallback, changeColorCallback, doubleClickCallback) {
  createTree(cellOrder, changeColorCallback, treePickingCallback);
  constructTreePicking(treePickingCallback, doubleClickCallback);
  if (treeState.merged == false) {
    createTopTree();
    constructTopTreePicking(treePickingCallback);
    createDots();
  }
  createSlider(callback);
  createThumbnail(cellOrder);
}

function updateLassoParent(leaves, sisterLeaves, mode) {
    treeModel.updateLassoParent(leaves, sisterLeaves, mode);
    treeModel.updateConstrains();
    return true;
}

function deleteItemFromTopTree(cell) {
    for (let lp of treeState.lassoParent) {
        if (lp.leaves.includes(cell)) {
            const index = lp.leaves.indexOf(cell);
            lp.leaves.splice(index, 1);
        }
    }
    for (let constrain of treeState.predictionConstrains) {
        if (constrain.includes(cell)) {
            const index = constrain.indexOf(cell);
            constrain.splice(index, 1);
        }
    }
}

function changeColors(colorMode, tempColor) {
    changeTreeColors(colorMode, tempColor);
    changeInstructionsColor(tempColor);
}

// change the tree colors
function changeTreeColors(colorMode, tempColor){
    let focusView = d3.select("svg#mainsvg").select("g");
    let thumbnails = d3.selectAll("g.thumbnails");
    if (colorMode == "confidence" || colorMode == "area" || colorMode == "random") {
        focusView.selectAll('rect')
            .attr("fill", function(d,i) {
                var name;
                if (d.name == undefined) {name = d;}
                else {name = d.name;}
                return utils.rgbToHex(cell3dState.colors[cell3dState.existingCells.indexOf(name)])});
        thumbnails.selectAll('rect')
            .attr("fill", function(d, i) {
                    let name;
                    if (d.name == undefined) {name = d;}
                    else {name = d.name;}
                    if (cell3dState.cellOrder.includes(name)) {
                        return utils.rgbToHex(cell3dState.colors[cell3dState.existingCells.indexOf(name)]);
                    } else if (tempColor.length > 0){
                        return tempColor[1];
                    } else {
                        const color = utils.averageColors(d.leaves, cell3dState.existingCells, cell3dState.colors);
                        return utils.rgbToHex(color);
                    }
                });
        d3.select("svg#topTree").selectAll('rect')
            .attr("fill", function(d, i) {
                if (d != undefined) {
                    if (tempColor.length > 0){ return tempColor[1];}
                    else {
                        const color = utils.averageColors(d.leaves, cell3dState.existingCells, cell3dState.colors);
                        return utils.rgbToHex(color);
                    }
                }else {return utils.rgbToHex([0,0,1]);}
            });
    }
    if (colorMode == "district") {
        focusView.selectAll('rect')
            .attr("fill", function(d,i) {
                var name;
                if (d.name == undefined) {name = d;}
                else {name = d.name;}
                return utils.rgbToHex(treeModel.nodeColor(cell3dState.cellOrder.indexOf(name)))});
        thumbnails.selectAll('rect')
            .attr("fill", function(d, i) {
                let name;
                if (d.name == undefined) {name = d;}
                else {name = d.name;}                
                if (cell3dState.cellOrder.includes(name)) {
                    return utils.rgbToHex(treeModel.nodeColor(cell3dState.cellOrder.indexOf(name)));
                } else {
                    const color = utils.averageColors(d.leaves, cell3dState.existingCells, cell3dState.colors);
                    return utils.rgbToHex(color);
                }
            });
        d3.select("svg#topTree").selectAll('rect')
            .attr("fill", function(d, i) {
                if (d != undefined) {
                    const color = utils.averageColors(d.leaves, cell3dState.existingCells, cell3dState.colors);
                    return utils.rgbToHex(color);
                } else {return utils.rgbToHex([0,0,1]);}
            });
    }
}

function changeInstructionsColor(tempColors) {
    d3.select("#instructions").selectAll("*").remove();
  const instructionWidth = interfaceState.svgWidth*interfaceState.smallWindowWidthRatio, instructionHeight = 48;
  var svg = d3.select("#instructions")
      .append("svg")
      .attr('width',instructionWidth+10)
      .attr('height',instructionHeight);
  var grad = svg.append('defs')
      .append("svg:linearGradient")
      .attr('id', 'grad')
      .attr('x1', '0%')
      .attr('x2', '100%')
      .attr('y1', '100%')
      .attr('y2', '100%')
      .attr("spreadMethod", "pad");
  if (tempColors.length > 1) {
      grad.append('stop')
          .attr("offset", "0%")
          .attr("stop-color", tempColors[0])
          .attr("stop-opacity", 1);
      grad.append('stop')
          .attr("offset", "100%")
          .attr("stop-color", tempColors[1])
          .attr("stop-opacity", 1);
      svg.append('rect')
          .attr('x',0)
          .attr('y',0)
          .attr('width', instructionWidth)
          .attr('height', 20)
          .style('fill', 'url(#grad)');

      var y = d3.scaleLinear()
          .range([instructionWidth-10, 10])
          .domain([1, 0]);
    
      var yAxis = d3.axisBottom()
          .scale(y)
          .ticks(3);
    
      svg.append("g")
          .attr("class", "y axis")
          .attr("transform", "translate(0,20)")
          .call(yAxis)
          .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 0)
          .attr("dy", ".71em");
  }
}

function moveTree(name) {
    var index1 = cell3dState.cellOrder.indexOf(name);
    var moveDistance = treeState.nodesXs[index1];
    var difference = treeState.nodeWidths[index1]/2;
    var thumb_rectX = Math.max(0, Math.min(interfaceState.svgWidth-interfaceState.svgWidth*treeState.diff, (difference+moveDistance-interfaceState.svgWidth/2)*treeState.diff));
    let focusView = d3.select("svg#mainsvg").select("g");
    treeState.focusMoved = thumb_rectX;
    d3.select("svg.thumbnail_svg")
        .select("rect.range")
        .attr("x", function (d, i){
            return thumb_rectX;
        });

    focusView.selectAll("rect")
        .attr("x", function(d, i){
            if (d.name == undefined) {var index = cell3dState.cellOrder.indexOf(d);}
            else {var index = cell3dState.cellOrder.indexOf(d.name);}
            var temp = treeState.nodesXs[index]-thumb_rectX/treeState.diff;
            return temp;
        });
    focusView.selectAll("text")
        .attr("x", function(d,i) {
            if (d.name == undefined) {var index = cell3dState.cellOrder.indexOf(d);}
            else {var index = cell3dState.cellOrder.indexOf(d.name);}
            var temp = treeState.nodesXs[index] + treeState.nodeWidths[index]/2- thumb_rectX/treeState.diff;
            return temp;
        });
    if (treeState.backgroundCorrespondingChildren.length > 0) {
        treeState.background.selectAll(".lines")
            .attr("x", function(d, i){
                const children = treeState.backgroundCorrespondingChildren[i]["name"];
                const child1 = children[0], 
                    child2 = children[1];
                const index1 = cell3dState.cellOrder.indexOf(child1),
                    index2 = cell3dState.cellOrder.indexOf(child2);
                var temp = Math.min(treeState.nodesXs[index1], treeState.nodesXs[index2]) - thumb_rectX/treeState.diff;
                return temp;
            });
        }
    if (treeState.lassoParent != null) {
        let topTreeFocused = d3.select("svg#topTree").select("g");
        topTreeFocused.selectAll("rect")
            .attr("x", function(d, i) {
                let tmp = treeModel.lassoX(i) - thumb_rectX/treeState.diff;
                return tmp;
            });
        topTreeFocused.selectAll("text")
            .attr("x", function(d, i) {
                let tmp = treeModel.lassoX(i) + treeModel.lassoWidth(i)/2 - thumb_rectX/treeState.diff;
                return tmp;
            })
    }
    if (treeState.marked != null && treeState.marked != name) {
        focusView.selectAll("rect")
            .filter(function(d,i){
                if (d.name == undefined) {return d == treeState.marked;}
                else {return d.name == treeState.marked;}
            })
            .style("stroke", "none");
    }
    treeState.marked = name;
    focusView.selectAll("rect")
        .filter(function(d,i){
            if (d.name == undefined) {return d == treeState.marked;}
            else {return d.name == treeState.marked;}
        })
        .style("stroke", "red")
        .style("stroke-width", 2);
}

function exportTextFile(filename, text) {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
  
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

function exportCsvFile(filename, rows) {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + rows.map(e => e.join(",")).join("\n"));
    element.setAttribute('download', filename);
  
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

module.exports = {
  createInstructions,
  createTopButtons,
  createZoomSlider,
  addTreeBackgroundColor,
  clear,
  clearTopTree,
  update,
  updateLassoParent,
  deleteItemFromTopTree,
  changeColors,
  moveTree,
  highlightCellinTree,
  confirmAllCurrentPairs,
  exportTextFile,
  exportCsvFile
}