const treeState = require('./treeState');
const cell3dState = require('./cell3dState');
const interfaceState = require('./interfaceState');
const utils = require('./utils');

function initLevels(assignmentData) {
  for (const cell of assignmentData) {
      if (cell.depth.includes(1)) {
          treeState.level1.push(cell.name);
      }
      // console.log(cell.depth);
      for (var j of cell.depth) {
          while (treeState.levelNum.length < j){
              treeState.levelNum.push(0);
              treeState.levelEmptyNum.push(0);
          }
          treeState.levelNum[j-1] ++;
      }
      if (cell.parent == 0) {
          treeState.levelEmptyNum[j-1] ++;
      }
  }
}

// calculate the first level cells normalization
function normalizeVolume() {
  var former = 0; // The previous level counts
  var array = [];
  for (var i of treeState.level1) {
      var index = cell3dState.existingCells.indexOf(i);
      array.push(cell3dState.volumes[index]);
  }
  treeState.normalizedVolumes = utils.normalizationArr(array);
}

function update(cellData) {
  initLevels(cellData);
  normalizeVolume();
}

function updateFromOrder(cellOrder){
  // TODO: create a dedicated function
  treeState.level1 = [];
  treeState.levelNum = [];
  treeState.levelEmptyNum = [];
  treeState.nodeWidths = [];
  treeState.cellColors = [];
  treeState.focusMoved = 0;
  treeState.nodesXs = [];
  for (var cell of cellOrder) {
      if (cell3dState.timeDepths[cell3dState.cellOrder.indexOf(cell)].includes(1)) {
          treeState.level1.push(cell);
      }
      // console.log(cell.depth);
      for (var j of cell3dState.timeDepths[cell3dState.cellOrder.indexOf(cell)]) {
          while (treeState.levelNum.length < j){
              treeState.levelNum.push(0);
              treeState.levelEmptyNum.push(0);
          }
          treeState.levelNum[j-1] ++;
      }
      if (cell3dState.assignedParent[cell3dState.cellOrder.indexOf(cell)] == 0) {
          treeState.levelEmptyNum[j-1] ++;
      }
  }
  normalizeVolume();
}

function readNewAssignment(assignmentData) {
  cell3dState.assignment = [];
  var num = '';
  for (var i of assignmentData) {
      if (i == ' ' && num != '') {
          cell3dState.assignment.push(parseInt(num));
          num = '';
      }
      else if (i == ' ') {continue;}
      else{   num += i;   }
  }
  for (var i of cell3dState.assignment) {
      if (i != 0 && !cell3dState.existingCells.includes(i)) {
          cell3dState.existingCells.push(i);
      }
  }
  cell3dState.existingCells.sort(function(a, b){return a - b});
  var removeValFromIndex = [];
  for (var i = 0; i < cell3dState.existingCells[cell3dState.existingCells.length-1]; i++){
      if (!cell3dState.existingCells.includes(i+1)){
          removeValFromIndex.push(i);
      }
  }
  for (var i = removeValFromIndex.length-1; i >=0; i--) {
      cell3dState.assignment.splice(removeValFromIndex[i], 1);
  }
  console.log(cell3dState.existingCells);
  console.log(cell3dState.assignment);
}

// get the index order
function getIndexInLevel(i) {
  var formerCellId = [];
  // if (d.depth.length == 1) {
      for (var m = 0; m < i; m++) {
          if (cell3dState.timeDepths[m].includes(cell3dState.timeDepths[i][0])){
              formerCellId.push(m);
          }
      }
      return formerCellId;
}

function nodeX(i) {
  if (cell3dState.timeDepths[i].includes(1)) {
      var formerCellId = getIndexInLevel(i);
      var x = 0;
      for (var j of formerCellId) {
          x += treeState.nodeWidths[j] + interfaceState.nodeHorizontalMargin;
      }
      // console.log(i, nodesXs.length);
      if (i >= treeState.nodesXs.length) {treeState.nodesXs.push(x);}
  }
  // else, then use the first/second cell3dState.children's x
  else {
      var x1 = nodeX(cell3dState.cellOrder.indexOf(cell3dState.children[i][0]));
      var x2 = nodeX(cell3dState.cellOrder.indexOf(cell3dState.children[i][1]));
      var x = Math.min(x1, x2);
      if (i >= treeState.nodesXs.length) {treeState.nodesXs.push(x);}
  }
  return x;
}

function nodeY(i) {
  return (interfaceState.nodeDefaultHeight*(treeState.levelNum.length - Math.max(...cell3dState.timeDepths[i])));
}

function nodeHeight(d) {
  if (d.depth == undefined) {
      return cell3dState.timeDepths[cell3dState.cellOrder.indexOf(d)].length*interfaceState.nodeDefaultHeight-interfaceState.nodeVerticalMargin;
  }
  return d.depth.length*interfaceState.nodeDefaultHeight-interfaceState.nodeVerticalMargin;
}

function nodeWidth(i) {
  var temp = 0;
  if (cell3dState.timeDepths[i][0] == 1) {
      var index = treeState.level1.indexOf(cell3dState.cellOrder[i]);
      temp = interfaceState.nodeDefaultWidth*(1+treeState.normalizedVolumes[index]);
  }
  else {
      var children_index1 = cell3dState.cellOrder.indexOf(cell3dState.children[i][0]),
          children_index2 = cell3dState.cellOrder.indexOf(cell3dState.children[i][1]);
      // console.log(i, children_index1, children_index2);
      temp = nodeWidth(children_index1) + nodeWidth(children_index2) + interfaceState.nodeHorizontalMargin;
  }
  if (treeState.nodeWidths.length <= i) {
      treeState.nodeWidths.push(temp);
  }
  return temp;
}

function editPossibilities(sister1, sister2, pre_sister1, pre_sister2){
  var changedColor = [1,0.86,0];
  // confirm that these two are sisters
  if (pre_sister1 == -1 && pre_sister2 == -1) {
      for (var i of cell3dState.pair_possibilities) {
          if (i['pairs'].includes(sister1) && i['pairs'].includes(sister2)) {
              i['possibility'] = 1;
          }
      }
      changedColor = [0.28,0.76,0.44];
  }
  else if (pre_sister1 == 0 && pre_sister2 == 0) {
      cell3dState.pair_possibilities.push({});
      cell3dState.pair_possibilities[cell3dState.pair_possibilities.length-1]['pairs'] = [sister1, sister2];
      cell3dState.pair_possibilities[cell3dState.pair_possibilities.length-1]['possibility'] = 1;
  }
  else if (pre_sister1 == 0) {
      for (var i of cell3dState.pair_possibilities) {
          if (i['pairs'].includes(sister2) && i['pairs'].includes(pre_sister2)) {
              i['possibility'] = 1;
          }
          i['pairs'] = [sister1, sister2];
      }
  }
  else if (pre_sister2 == 0) {
      for (var i of cell3dState.pair_possibilities) {
          if (i['pairs'].includes(sister1) && i['pairs'].includes(pre_sister1)) {
              i['possibility'] = 1;
          }
          i['pairs'] = [sister1, sister2];
      }
  }
  // change the assignments       
  else {
      for (var i of cell3dState.pair_possibilities) {
          if (i['pairs'].includes(sister1) || i['pairs'].includes(sister2)){
              cell3dState.pair_possibilities.slice(cell3dState.pair_possibilities.indexOf(i),1);
          }
      }
      cell3dState.pair_possibilities.push({});
      cell3dState.pair_possibilities[cell3dState.pair_possibilities.length-1]['pairs'] = [sister1, sister2];
      cell3dState.pair_possibilities[cell3dState.pair_possibilities.length-1]['possibility'] = 1;
      cell3dState.pair_possibilities.push({});
      cell3dState.pair_possibilities[cell3dState.pair_possibilities.length-1]['pairs'] = [pre_sister1, pre_sister2];
      cell3dState.pair_possibilities[cell3dState.pair_possibilities.length-1]['possibility'] = 0.3;
  }
  changeAssignedColor(changedColor, sister1, sister2);
}

// only change 
function changeAssignedColor(changedColor, sister1, sister2) {
  cell3dState.colors_confidence[cell3dState.existingCells.indexOf(sister1)] = changedColor;
  cell3dState.colors_confidence[cell3dState.existingCells.indexOf(sister2)] = changedColor;
  for (var k of findAllNodesBelow(sister1)) {
      cell3dState.colors_confidence[cell3dState.existingCells.indexOf(k)] = changedColor;
  }
  for (var k of findAllNodesBelow(sister2)) {
      cell3dState.colors_confidence[cell3dState.existingCells.indexOf(k)] = changedColor;
  }
}

// find all nodes below
function findAllNodesBelow(name) {
  const index = cell3dState.cellOrder.indexOf(name);
  if (cell3dState.children[index] == 0) {return [name];}
  else {
      let node = [name];
      node = node.concat(findAllNodesBelow(cell3dState.children[index][0]));
      node = node.concat(findAllNodesBelow(cell3dState.children[index][1]));
      return node;
  }    
}

function deleteAllParentAbove(pre_sister1, pre_sister2) {
  var parents = [];
  var parent = cell3dState.assignment[cell3dState.existingCells.indexOf(pre_sister2)];
  cell3dState.children[cell3dState.cellOrder.indexOf(parent)] = [pre_sister1 , pre_sister2];
  while (parent != 0) {
      parents.push(parent);
      parent = cell3dState.assignment[cell3dState.existingCells.indexOf(parent)];
  }
  for (var i of parents) {
      var child1 = cell3dState.children[cell3dState.cellOrder.indexOf(i)][0];
      var child2 = cell3dState.children[cell3dState.cellOrder.indexOf(i)][1];
      cell3dState.assignment[cell3dState.existingCells.indexOf(child1)] = 0;
      cell3dState.assignment[cell3dState.existingCells.indexOf(child2)] = 0;
  }
  var parentsIndex = [];
  for (var i of parents) {
      parentsIndex.push(cell3dState.existingCells.indexOf(i));
  }
  for (var i = parents.length-1; i >=0 ; i--) {
      for (var j = cell3dState.existingCells.indexOf(parents[i]); j < cell3dState.assignment.length-1; j++) {    
          cell3dState.assignment[j] = cell3dState.assignment[j+1];
      }
      cell3dState.assignment.pop();
      cell3dState.existingCells.splice(cell3dState.existingCells.indexOf(parents[i]),1);
  }
  console.log(cell3dState.assignment);
  for (var i = 0; i < cell3dState.assignment.length; i++) {
      for (var j = parents.length-1; j >=0; j--) {
          if (cell3dState.assignment[i] > parents[j]) {
              cell3dState.assignment[i] -= j+1;
              break;
          }
      }
  }
  for (var i = 0; i < cell3dState.existingCells.length; i++) {
      for (var j = parents.length-1; j >=0; j--) {
          if (cell3dState.existingCells[i] > parents[j]) {
              cell3dState.existingCells[i] -= j+1;
              break;
          }
      }
  }
}

function nodeColor(i){
    if (cell3dState.timeDepths[i].includes(1)) {
        var index = cell3dState.existingCells.indexOf(cell3dState.cellOrder[i]);
        if (treeState.cellColors.length <= i) {treeState.cellColors.push(utils.rgbToHex(cell3dState.colors[index]));}
        return cell3dState.colors[index];
    }
    else {
        var color1 = nodeColor(cell3dState.cellOrder.indexOf(cell3dState.children[i][0]));
        var color2 = nodeColor(cell3dState.cellOrder.indexOf(cell3dState.children[i][1]));
        var color12 = utils.averageColorOfTwo(color1, color2);
        if (treeState.cellColors.length <= i) {treeState.cellColors.push(utils.rgbToHex(color12));}
        return color12;
    }
}

function greyUncertainty(pre_sister1, pre_sister2) {
    colors_confidence[cell3dState.existingCells.indexOf(pre_sister1)] = [0.6,0.6,0.6];
    colors_confidence[cell3dState.existingCells.indexOf(pre_sister2)] = [0.6,0.6,0.6];
    for (var k of findAllNodesBelow(pre_sister1)) {
        colors_confidence[cell3dState.existingCells.indexOf(k)] = [0.6,0.6,0.6];
    }
    for (var k of findAllNodesBelow(pre_sister2)) {
        colors_confidence[cell3dState.existingCells.indexOf(k)] = [0.6,0.6,0.6];
    }
}

function editAssignments() {
  const cell1 = cell3dState.highlighted,
        cell2 = cell3dState.neighboringTried;
  const index1 = cell3dState.cellOrder.indexOf(cell1);
  const parent1 = cell3dState.assignedParent[index1];
  let sister1 = [];
  if (parent1 != 0) {
      sister1 = cell3dState.children[cell3dState.cellOrder.indexOf(parent1)].slice();
      sister1.splice(sister1.indexOf(cell1), 1);
  }
  let tmpSister = cell3dState.assignment[cell3dState.existingCells.indexOf(cell2)];
  let parent2 = cell3dState.assignedParent[cell3dState.cellOrder.indexOf(cell2)];
  // all of them have no parents
  if (sister1.length == 0 && parent2 == 0) {
      cell3dState.assignment.push(0);
      var toCreate = cell3dState.existingCells[cell3dState.existingCells.length-1] +1;
      cell3dState.existingCells.push(toCreate);
      cell3dState.assignment[cell3dState.existingCells.indexOf(cell1)] = toCreate;
      cell3dState.assignment[cell3dState.existingCells.indexOf(cell2)] = toCreate;
      editPossibilities(cell1, cell2, 0, 0);
  }
  else if (sister1.length == 0 && parent2 != 0){
      var sister2 = cell3dState.children[cell3dState.cellOrder.indexOf(parent2)].slice();
      sister2.splice(sister2.indexOf(cell2),1);
      cell3dState.assignment[cell3dState.existingCells.indexOf(sister2[0])] = 0;
      cell3dState.assignment[cell3dState.existingCells.indexOf(cell1)] = cell3dState.assignment[cell3dState.existingCells.indexOf(cell2)];
      editPossibilities(cell1, cell2, 0, sister2[0]);
  }
  else if (sister1.length > 0 && parent2 == 0) {
      cell3dState.assignment[cell3dState.existingCells.indexOf(sister1[0])] = 0;
      cell3dState.assignment[cell3dState.existingCells.indexOf(cell2)] = cell3dState.assignment[cell3dState.existingCells.indexOf(cell1)];
      editPossibilities(cell1, cell2, sister1[0], 0);
  }
  else {
      var sister2 = cell3dState.children[cell3dState.cellOrder.indexOf(parent2)].slice();
      sister2.splice(sister2.indexOf(cell2),1);
      cell3dState.assignment[cell3dState.existingCells.indexOf(cell2)] = cell3dState.assignment[cell3dState.existingCells.indexOf(cell1)];
      cell3dState.assignment[cell3dState.existingCells.indexOf(sister1[0])] = tmpSister;
      deleteAllParentAbove(sister1[0], sister2[0]);
      editPossibilities(cell1, cell2, sister1[0], sister2[0]);
  }
}

// change the color in tree nodes (mark as checked)
function recordTreeBackgroundColor(cellArr, color) {
    let line = {};
    line["name"] = cellArr;
    line["color"] = color;
    treeState.backgroundCorrespondingChildren.push(line);
}

function getTotalWidthOfTree() {
    let totalWidth = 0;
    for (let i = 0; i < cell3dState.timeDepths.length; i++) {
        if (cell3dState.timeDepths[i].includes(1)){
            totalWidth += treeState.nodeWidths[i]+interfaceState.nodeHorizontalMargin;
        }
    }
    totalWidth -= interfaceState.nodeHorizontalMargin;
    return totalWidth;
}

// the children here could be grandchildren or children of grandchildren (just leaves)
function getWidthOfTopNodes(childrenArr) {
    let width = 0;
    for (let i of childrenArr) {
        const index = cell3dState.cellOrder.indexOf(i);
        width += treeState.nodeWidths[index]+interfaceState.nodeHorizontalMargin;
    }
    width -= interfaceState.nodeHorizontalMargin;
    return width;
}

function initialLassoParent(){
    treeState.lassoParent.push({});
    const lastIndex = treeState.lassoParent.length - 1;
    treeState.lassoParent[lastIndex]["name"] = "N";
    treeState.lassoParent[lastIndex]["depth"] = [treeState.levelNum.length+1+1];
    treeState.lassoParent[lastIndex]["leaves"] = utils.sortArrayDescending(cell3dState.existingCells.slice());
    treeState.lassoParent[lastIndex]["sister"] = null;
    treeState.lassoParent[lastIndex]["children"] = [];
    treeState.predictionConstrains.push(cell3dState.existingCells.slice());
}

function lassoWidth(i) {
    return getWidthOfTopNodes(treeState.lassoParent[i]["leaves"]);
}

function lassoHeight(i) {
    const height = treeState.lassoParent[i].depth.length*interfaceState.nodeDefaultHeight-interfaceState.nodeVerticalMargin;
    return height;
}

function lassoX(i) {
    let xs = [];
    for (let leave of treeState.lassoParent[i].leaves) {
        xs.push(nodeX(cell3dState.cellOrder.indexOf(leave)));
    }
    return Math.min(...xs);
}

function lassoY(i) {
    // const depth = Math.round(Math.log2(cell3dState.existingCells.length))+1;
    const depth = Math.max(...treeState.lassoParent[0].depth);
    return (interfaceState.nodeDefaultHeight*(depth - Math.max(...treeState.lassoParent[i].depth)));
}

function updateLassoParent(leaves, sisterLeaves, mode) {
    if (mode == "lassoSelect") {
        const parentIndex = utils.checkIteminObjectArray(cell3dState.highlighted, treeState.lassoParent, "name");
        const parentDepth = treeState.lassoParent[parentIndex].depth;
        let addLayer = true;
        for (let lasso of treeState.lassoParent) {
            if (lasso.depth[0] == parentDepth-1) {addLayer = false; break;}
        }
        treeState.lassoParent.push({});
        let lastIndex = treeState.lassoParent.length - 1;
        treeState.lassoParent[lastIndex]["name"] = "N-"+(lastIndex).toString();
        treeState.lassoParent[lastIndex]["depth"] = [parentDepth-1];
        treeState.lassoParent[lastIndex]["leaves"] = leaves;
        treeState.lassoParent[lastIndex]["sister"] = lastIndex+1;
        treeState.lassoParent[lastIndex]["children"] = [];
        treeState.lassoParent[parentIndex]["children"].push("N-"+(lastIndex).toString());
        treeState.lassoParent.push({});
        // lastIndex has changed since we pushed in
        lastIndex = treeState.lassoParent.length - 1;
        treeState.lassoParent[lastIndex]["name"] = "N-"+(lastIndex).toString();
        treeState.lassoParent[lastIndex]["depth"] = [parentDepth-1];
        treeState.lassoParent[lastIndex]["leaves"] = sisterLeaves;
        treeState.lassoParent[lastIndex]["sister"] = lastIndex-1;
        treeState.lassoParent[lastIndex]["children"] = [];
        treeState.lassoParent[parentIndex]["children"].push("N-"+(lastIndex).toString());
        // When the new layer is generated, the depths of every node should be changed
        if (addLayer == true) {
            for (let lasso of treeState.lassoParent) {
                lasso.depth = [lasso.depth[0]+1];
            }
        }
        
    } else {
        const targetIndex = utils.checkIteminObjectArray(cell3dState.highlighted, treeState.lassoParent, "name");
        const sisterIndex = treeState.lassoParent[targetIndex]["sister"];
        treeState.lassoParent[targetIndex]["leaves"] = leaves;
        treeState.lassoParent[sisterIndex]["leaves"] = sisterLeaves;
    }
}

function reorderTree() {
    const length = treeState.lassoParent.length;
    let toInsert = [];
    for (let len = length-2; len <= length-1; len++) {
        toInsert = toInsert.concat(treeState.lassoParent[len].leaves);
    }
    const oldOrder = cell3dState.cellOrder.slice()
    for (let cell of oldOrder) {
        if (toInsert.includes(cell)) {
            const index = cell3dState.cellOrder.indexOf(cell);
            cell3dState.cellOrder.splice(index,1);
        }
    }
    cell3dState.cellOrder = toInsert.concat(cell3dState.cellOrder);
}

function updateConstrains() {
    let constrains = [];
    const length = treeState.lassoParent.length;
    for (let i = length-1; i >= 0; i--) {
        const leaves = treeState.lassoParent[i].leaves;
        if (!utils.elementIncluded(constrains, leaves)) {
            constrains.push(leaves);
        }
    }
    treeState.predictionConstrains = constrains;
}

module.exports = {
  update,
  updateFromOrder,
  readNewAssignment,
  nodeX,
  nodeY,
  nodeHeight,
  nodeWidth,
  editPossibilities,
  deleteAllParentAbove,
  editAssignments,
  nodeColor,
  recordTreeBackgroundColor,
  getTotalWidthOfTree,
  initialLassoParent,
  lassoWidth,
  lassoHeight,
  lassoX,
  lassoY,
  updateLassoParent,
  reorderTree,
  updateConstrains
}