"use strict";

class DatasetState{
    constructor() {
        this.init();
    }

    init() {
        this.tissuesTriangles = [];
        this.tissues = null;
        this.trianglesInfo = null;
    }

    clear() {
        this.init();
    }
}

const datasetState = new DatasetState();

module.exports = datasetState;