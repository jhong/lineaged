const fetches = require('./fetches');
const cell3dState = require('./cell3dState');
const treeState = require('./treeState');
const cell3dController = require('./cell3dController');
const cell3dView = require('./cell3dView');
const cell3dModel = require('./cell3dModel');
const treeController = require('./treeController');
const treeModel = require('./treeModel');
const treeView = require('./treeView');
const interactionsBetween = require('./interactionsBetween');
const utils = require('./utils');
const studyState = require('./studyState');

// polling function
async function jobStatusPoll(waitingTime, jobId) {
    const status = await fetches.getJobStatus(jobId);
    if (status != "Done") {
        setTimeout(() => {
            jobStatusPoll(waitingTime, jobId)
        }, waitingTime);
    } else {
        // TODO: update the tree
        await treeControllerAddOneLevel();
    }
}

async function selectDefaultDataSet(menuIndex) {
    // TODO: another test to see if state is set
    // and that rendering already took place ?
    if (treeState.levelNum.length != 0) {
        treeController.clearAll();
        cell3dController.clearAll();
    } else {
        cell3dView.createExplosionSlider();
        treeView.createTopButtons(interactionsBetween.changeColor);
        treeView.createInstructions();
        
    }

    let cellDataUrl;
    let cell3dDataUrl;
    switch (menuIndex) {
        case 3:
            cellDataUrl = '/data/assignment16c_Col0_654.json';
            cell3dDataUrl = '/data/parsed16c_Col0_654.json';
            break;
        case 4:
            cellDataUrl = '/data/assignment32-64c_Col0_977.json';
            cell3dDataUrl = '/data/parsed32-64c_Col0_977.json';
            break;
        case 5:
            cellDataUrl = '/data/assignmentcoeur_Col0_915.json';
            cell3dDataUrl = '/data/parsedcoeur_Col0_915.json';
            break;
        default:
            console.log('not corresponding default dataset found');
    }

    const cellData = await fetches.fetchJson(cellDataUrl);
    const cell3dData = await fetches.fetchJson(cell3dDataUrl);

    studyState.startingTime = performance.now();    
    cell3dModel.create(cell3dData, cellData);
    if (treeState.levelNum.length == 0) {cell3dView.createPeelingSlider();}
    cell3dView.update(interactionsBetween.pickOnMouseDownEvent, interactionsBetween.pickOnMouseEventOverview);
    
    // TODO: hidden depencency to cell3dModelUpdate
    // side effects on cell3dState.volumes and
    // cell3dState.timeDepths
    treeModel.update(cellData);
    treeModel.initialLassoParent();
    treeView.update(cellData, cell3dView.switchToLevel, interactionsBetween.highlightCell, interactionsBetween.changeColor, interactionsBetween.doubleClickReaction);
    treeView.createZoomSlider(interactionsBetween.freshTree);
}

async function processCommand(value) {
    if (value == 0 || value == 1 || value == 2) {
        // not implemented yet server side
        await fetches.sendCommandsToServer(value);
        if (value == 1) {
            const exported = utils.extendedAssignmentsToExport(cell3dState.existingCells, cell3dState.assignment);
            treeView.exportTextFile("assignment.txt", exported.toString());
            // treeView.exportCsvFile("study.csv", studyState.actionRow);
        }
    }
    if (value == 3 || value == 4 || value == 5) {
        // defaults dataset
        await fetches.sendCommandsToServer(value);

        cell3dView.addLoader();
        await selectDefaultDataSet(value);
        cell3dView.removeLoader();
    }
    if (value == 6 || value == 7) {
        treeView.confirmAllCurrentPairs();
        if (treeState.merged == false) {
            // predict single level
            const jobId = await fetches.submitOneLevelPrediction();
            await jobStatusPoll(1000, jobId);
        }
        
    }
}

function listenNav() {
    // users choose one embryo dataset
    // querySelectorAll returns a NodeList, which has forEach, even if it is not an Array
    document.querySelectorAll('.menu_option').forEach((item, index) => {
        item.onclick = async function() {
            console.log(index);
            await processCommand(index);
        }
    });

    document.onkeyup = function(e) {
        if (treeState.clickDivideStatus == true && e.key == "Enter") {
            document.getElementById(clickSelection).style.border = "2px solid black";
            reformDivision(treeState.clickDivideCells, clickSelection);
            treeState.clickDivideCells = [];
            treeState.clickDivideStatus = false;
        }
    }
}

// Ensure DOM is fully loaded and parsed
window.addEventListener('DOMContentLoaded', (event) => {
    listenNav();
});

interactionButtonsSetting();
cell3dView.settingLasso();
let lassoSelectVar = false;
let clickSelection = null;

function lassoSelectionReply() {
    if (lassoSelectVar == false) {
        lassoSelectVar = true;
        cell3dView.startingLasso();
        document.getElementById(this.id).style.border = "2px solid red";
    } else if (lassoSelectVar == true) {
        lassoSelectVar = false; 
        document.getElementById(this.id).style.border = "2px solid black";
        const intersectedCells = cell3dView.lassoSelect();
        cell3dView.removingLasso();
        reformDivision(intersectedCells, this.id);
    }
}

function clickSelectionReply() {
    clickSelection = this.id;
    document.getElementById(this.id).style.border = "2px solid red";
    treeState.clickDivideStatus = true;
}

async function reformDivision(intersectedCells, id){
    if (cell3dState.highlighted == null || 
        !isNaN(cell3dState.highlighted)) {alert("Select the right cell to divide."); return;}
    const [leaves, sisterLeaves] = checkTempLassoParent(intersectedCells, id);
    if (leaves == null) {return;}
    const flag = treeView.updateLassoParent(leaves, sisterLeaves, id);
    if (flag == true) {
        if (treeState.levelNum.length > 1) {cell3dModel.freshData();}
        else {treeModel.reorderTree();}
        interactionsBetween.freshTree();
        await fetches.predictionConstrainsUpdate(treeState);
    }
}

function interactionButtonsSetting() {
    const confirmButtons = document.querySelectorAll("#confirmButton");
    for (let confirmButton of confirmButtons) {
        confirmButton.onclick = function() {confirmCurrentAssign()};
    }

    const wrongChildButtons = document.querySelectorAll("#wrongChildButton");
    for (let wrongChildButton of wrongChildButtons) {
        wrongChildButton.onclick = function() {childrenUnlink(cell3dState.highlighted)};
    }

    const newSisterButtons = document.querySelectorAll("#newSisterButton");
    for (let newSisterButton of newSisterButtons) {
        newSisterButton.onclick = async function() {await setAsNewSister()};
    }

    const supportingButtons = document.querySelectorAll("#supportingButton");
    for (let supportingButton of supportingButtons) {
        supportingButton.onclick = async function() {await setAsSupportingCells(cell3dState.highlighted);};
    }

    const lineDivide = document.getElementById("lassoSelect");
    const addToSelection = document.getElementById("lassoSelectAdd");
    const subtractSelection = document.getElementById("lassoSelectSubtract");

    lineDivide.onclick = lassoSelectionReply;
    addToSelection.onclick = lassoSelectionReply;
    subtractSelection.onclick = lassoSelectionReply;

    const clickAdd = document.getElementById("clickSelectAdd");
    const clickSubtract = document.getElementById("clickSelectSubtract");

    clickAdd.onclick = clickSelectionReply;
    clickSubtract.onclick = clickSelectionReply;
}

function confirmCurrentAssign() {
    let children = cell3dState.children[cell3dState.cellOrder.indexOf(cell3dState.highlighted)];
    if (children != 0 && children.length == 2){
        let sister1 = children.slice()[0];
        treeView.addTreeBackgroundColor(sister1, "green", true);
        treeModel.recordTreeBackgroundColor(children.slice(), "green");
    }
}

function childrenUnlink(parentName) {
    if (treeState.merged == true) {return;}
    // change the assignments
    if (parentName != null) {
        let children = cell3dState.children[cell3dState.cellOrder.indexOf(parentName)];
        if (children != undefined && children != 0 && children.length == 2){
            let sister1 = children.slice()[0],
                sister2 = children.slice()[1];
            treeModel.deleteAllParentAbove(sister1, sister2);
            // reorder the tree
            fresh3dData();
            interactionsBetween.freshTree();
        }
    }
}

async function setAsNewSister() {
    if (treeState.merged == true) {return;}
    if (cell3dState.neighboringTried != null && cell3dState.highlighted != null && cell3dState.neighboringTried != cell3dState.highlighted) {
        const leaves1 = cell3dModel.findAllLeaves(cell3dState.highlighted);
        const leaves2 = cell3dModel.findAllLeaves(cell3dState.neighboringTried);
        // Tell whether two sisters are from the same group
        for (let constrain of treeState.predictionConstrains) {
            if ((utils.isSuperset(constrain, leaves1) && !utils.isSuperset(constrain, leaves2)) ||
            (!utils.isSuperset(constrain, leaves1) && utils.isSuperset(constrain, leaves2))) {
                alert("You have choose two cells from different parts!");return;
            }
        }
        const highlight = cell3dState.highlighted;
        treeModel.editAssignments();
        fresh3dData();
        await fetches.updateGlobalAssignments(cell3dState);
        interactionsBetween.freshTree();
        treeView.moveTree(cell3dState.neighboringTried);
        treeModel.recordTreeBackgroundColor([highlight, cell3dState.neighboringTried], "orange");
        interactionsBetween.changeToMainView();
        treeView.addTreeBackgroundColor(highlight, "orange", true);
    }
}

async function setAsSupportingCells(supportingCell) {
    if (cell3dState.currentMode != "mainView") {alert("Please mark it from the main view."); return;}
    if (!cell3dState.defaultCells.includes(supportingCell)) {alert("Please choose supporting cells from the first level."); return;}
   if (confirm('Are you sure that you want to mark it as a supporting cell?')){
       console.log("Supporting:" + supportingCell);
        unlinkAssignedChild(supportingCell);
        cell3dState.supportingCells.push(supportingCell);        
        cell3dView.hideCell([supportingCell]);
        treeView.deleteItemFromTopTree(supportingCell);
        cell3dModel.deleteItemFromEmbryo(supportingCell);
        await fetches.updateGlobalSupportings(cell3dState);
        cell3dModel.updateColors();
        interactionsBetween.freshTree();
   }
}

function unlinkAssignedChild(name) {
    let parents = [];
    let index = cell3dState.cellOrder.indexOf(name);
    let parent = cell3dState.assignedParent[index];
    while (parent != 0) {
        parents.push(parent);
        index = cell3dState.cellOrder.indexOf(parent);
        parent = cell3dState.assignedParent[index];
    }
    for (let i = parents.length-1; i >= 0; i--) {
        childrenUnlink(parents[i]);
    }
}


function fresh3dData() {
    cell3dModel.freshData();
    cell3dView.clearTargetView();
    cell3dView.showCurrentCells();
    cell3dController.getPossibilities();
}

async function treeControllerAddOneLevel() {
    const previousCellOrder = cell3dState.cellOrder;
    const assignmentData = await fetches.fetchText('/dataRW/assignment_ml.txt');
    treeModel.readNewAssignment(assignmentData);
    await cell3dController.getPossibilities();
    cell3dModel.freshData();
    if (cell3dModel.checkWhetherMerge()) {
        interactionsBetween.mergeTwoTrees();
        treeState.merged = true;
        treeState.lassoParent = [];
    } else if (!utils.arraysEqual(previousCellOrder, cell3dState.cellOrder)){
        // When the new layer is generated, the depths of every node should be changed
        for (let lasso of treeState.lassoParent) {
            lasso.depth = [lasso.depth[0]+1];
        }
    }
    interactionsBetween.freshTree();
}

function checkTempLassoParent(intersectedCells, mode) {
    let groupA = [], groupB = [];
    let tmpHighlight = cell3dState.highlighted;
    // mode = new selection
    if (mode == "lassoSelect") {
        const parentIndex = utils.checkIteminObjectArray(cell3dState.highlighted, treeState.lassoParent, "name");
        const parentLeaves = treeState.lassoParent[parentIndex].leaves;
        const filteredArray = parentLeaves.filter(value => intersectedCells.includes(value));
        groupA = utils.sortArrayDescending(filteredArray);
        const sisterCells = utils.differenceOfSets(parentLeaves, filteredArray);
        groupB = utils.sortArrayDescending(sisterCells);
    } else {
        const targetIndex = utils.checkIteminObjectArray(cell3dState.highlighted, treeState.lassoParent, "name");
        let leaves = treeState.lassoParent[targetIndex].leaves.slice();
        const sisterIndex = treeState.lassoParent[targetIndex]["sister"];
        let sisterLeaves = treeState.lassoParent[sisterIndex].leaves.slice();
        // mode = add to selection
        if (mode == "lassoSelectAdd" || mode == "clickSelectAdd") {
            let toAdd = [];
            for (let cell of intersectedCells) {
                if (!leaves.includes(cell) && sisterLeaves.includes(cell)) {toAdd.push(cell);}
            }
            leaves = leaves.concat(toAdd);
            groupA = utils.sortArrayDescending(leaves);
            groupB = utils.differenceOfSets(sisterLeaves, toAdd);
        } else if (mode == "lassoSelectSubtract" || mode == "clickSelectSubtract") { // mode = subtract from selection
            let toRemove = [];
            for (let cell of intersectedCells) {
                if (leaves.includes(cell) && !sisterLeaves.includes(cell)) {toRemove.push(cell);}
            }
            groupA = utils.differenceOfSets(leaves, toRemove);
            sisterLeaves = sisterLeaves.concat(toRemove);
            groupB = utils.sortArrayDescending(sisterLeaves);
        }
    }
    // if the conflict, ask biologists whether they want to continue
    if (treeState.levelNum.length > 1) {
        const conflicted = checkConflictsInDivisionPrediction(groupA, groupB);
        if (conflicted.length > 0) {
            // if they confirm, then unlink all the conflicts
            if (confirm("The division is conflicted with the current assignments. Do you want to continue?")) {
                for (let conflictedItem of conflicted) {
                    childrenUnlink(conflictedItem);
                }
            } else {return [null,null];}
        }
    }
    cell3dState.highlighted = tmpHighlight;
    return [groupA, groupB];
}

function checkConflictsInDivisionPrediction(groupA, groupB) {
    let conflicted = [];
    for (let i = cell3dState.existingCells.length-1; i >= 0; i--) {
        const leaves = cell3dModel.findAllLeaves(cell3dState.existingCells[i]);
        if (utils.differenceOfSets(groupA, leaves).length == groupA.length && 
        utils.differenceOfSets(groupB, leaves).length == groupB.length) {continue;}
        if (!utils.isSuperset(groupA, leaves) && !utils.isSuperset(groupB, leaves)) {
            conflicted.push(cell3dState.existingCells[i]);
        }
    }
    return conflicted;
}

