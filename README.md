# Website for Cell Lineages

Website Link: https://plantembryo.saclay.inria.fr/ (Contact authors for the password)

**Cell Lineage Web** is the website allow biologists to freely do cell lineages with the help of Machine Learning. Find more information on our [project web](https://www.aviz.fr/Research/CellLineage).

Besides accessing directly to the website link, you could also run the website through your own local computer server as following the instructions below.

# Setting up the local server
To set up your own server, you need to first download the source code in terminal:

```
git clone https://gitlab.inria.fr/jhong/lineaged.git
```
Then enter the downloaded repository:
```
cd lineaged
```


## Installation
---

We used Node.js (v14.15.0) to create our backend server and npm (v6.14.8) to manage our node packages. To install them, 

1. if you are on macOS,

    you need to have homebrew on your computer. If not, install it as follows:
    ```
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)
    ```
    Then update the homebrew to the latest version:
    ```
    brew update
    ```
    Run the following command below in the terminal to install Node.js and npm:
    ```
    brew install node
    ```
2. If you are on Linux,

    you could directly install Node.js and npm as follows in the terminal:
    ```
    sudo apt install nodejs
    ```
    ```
    sudo apt install npm
    ```
Then, to install all the necessary packages we used in the website, simply run in the terminal:
```
npm install
```

## MongoDB

You'll need to have a mongodb instance running. You could use docker-compose for that end,
with a file like this (add user and password):

```
version: '3.1'

services:

  plantjs-mongodb:
    image: mongo
    environment:
      MONGO_INITDB_ROOT_USERNAME:
      MONGO_INITDB_ROOT_PASSWORD:
    ports:
      - "27017:27017"
```
You could follow the instructions here: https://www.mongodb.com/docs/manual/administration/install-on-linux/ based on your Linux version.

## Update the environment file

Copy the `.env.sample` file to `.env` and edit it where needed, e.g

* change the mongodb informations (host, database, user, password)
* change the session key


## Starting up
---
Once mongodb is running and reachable, you can start the backend (node) with:

```
npm run start
```

And on another terminal, start the frontend (parcel-bundler) with:

```
npm run frontend
```

## Browse

http://localhost:1234
