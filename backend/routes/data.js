const router = require('express').Router();
const path = require('path');

router.get('/assignment16c_Col0_654.json', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../data/" + "assignment16c_Col0_654.json"))
});

router.get('/assignment32-64c_Col0_977.json', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../data/" + "assignment32-64c_Col0_977.json"))
});

router.get('/assignmentcoeur_Col0_915.json', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../data/" + "assignmentcoeur_Col0_915.json"))
});

router.get('/parsedcoeur_Col0_915.json', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../data/" + "parsedcoeur_Col0_915.json"))
});

router.get('/parsed16c_Col0_654.json', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../data/" + "parsed16c_Col0_654.json"))
});

router.get('/parsed32-64c_Col0_977.json', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../data/" + "parsed32-64c_Col0_977.json"))
});

router.get('/saved_model/model.json', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../saved_model/" + "model.json"))
});

module.exports = router;