const router = require('express').Router();

const globalService = require('../services/global');

// WARNING: update and not create here
router.post('/', async function(req, res) {
    // check that the user has access to this ressource
    const sessionId = req.sessionID;
    const currentExistingCells = req.body.existingCells,
          currentAssignments = req.body.currentAssignments,
          addedSupportingCells = req.body.supportingCells;
    await globalService.newAssignmentsUpdate(sessionId, currentExistingCells, currentAssignments, addedSupportingCells);
    jsonResponse = {
        status: "updated"
    };
    res.status('200').json(jsonResponse);
});

router.post('/supporting', async function(req, res) {
    const sessionId = req.sessionID;
    const addedSupportingCells = req.body.supportingCells;
    await globalService.supportingUpdate(sessionId, addedSupportingCells);
    jsonResponse = {
        status: "updated"
    };
    res.status('200').json(jsonResponse);
})

router.post('/predictionConstrains', async function(req, res) {
    const sessionId = req.sessionID;
    const predictionConstrains = req.body.predictionConstrains;
    await globalService.predictionConstrainsUpdate(sessionId, predictionConstrains);
    jsonResponse = {
        status: "updated"
    };
    res.status('200').json(jsonResponse);
})

module.exports = router;