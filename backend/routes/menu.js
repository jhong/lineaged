const router = require('express').Router();

const datasetService = require('../services/dataset');
const modelService = require('../services/model');

router.get('/datasetName', function(req, res) {
    console.log("update");
    res.send(FILENAME);
})

// get dataset
router.post('/', function(req, res) {
    let [num] = req.body;
    dealCommands(num, req.sessionID);
    res.sendStatus(200);
});

// deal with different datasets
function dealCommands(index, sessionId) {
    console.log("commands:"+index);
    let filename = "None";

    // upload dataset
    if (index == 0) {

    }
    // export assignments
    if (index == 1) {
        
    }
    // select default dataset
    if (index >= 3 && index <= 5) {
        switch(index) {
            case 3:
                // 16 cells
                datasetName = '16c_Col0_654';
                break;
            case 4:
                // 64 cells
                datasetName = '32-64c_Col0_977';
                break;
            case 5:
                // 256 cells
                datasetName = 'coeur_Col0_915';
                break;
        }
        // modelService.clearModelGlobalVariables();
        datasetService.getParsedData(datasetName, sessionId);
    }
    
    // predict all
    if (index == 6) {
        modelService.predictAll(sessionId);
    }
    // predict one level (previous index 7) is now moved to the job endpoint

    // clear all
    if (index == 8) {
        
    }
}

// send the possibilities
router.get('/getPossibilities', async function(req, res) {
    const pairPossibilities = await modelService.getPairPossibilities(req.sessionID);
    res.json(pairPossibilities);
});

// app.get('/datasetsOpen', function(req, res) {
//     res
// });

router.post('/updateAssignments', (req,res) => {
    var body = [];
    req.on('data', (chunk) => {
            body.push(chunk);
        })
        .on('end', () => {
            body = Buffer.concat(body).toString();
            var [assignments] = JSON.parse(body);
            modelService.global_current_assignments = assignments;
        });
});

// retrain model
router.post('/trainOneRecord',  (req,res)=> {
  var body = [];
  req.on('data', (chunk) => {
    body.push(chunk);
  })
  .on('end', () => {
    // on end of data, perform necessary action
    body = Buffer.concat(body).toString();
    //parse two cell names from front end
    const [leaves1, leaves2, neighborCount1, neighborCount2, level]  = JSON.parse(body);
    //get the pair data for training from these two cells
    // TODO: function does not exist ?
    const record = getRecordFromCells(leaves1,leaves2,neighborCount1, neighborCount2, level);  
    try { 
        modelService.trainModel(record,1)
        .then(() => res.send(200))
        .catch(error => {
               res.send(404,''+error);  
               throw error;
            }
        );
    }
    catch(error){
        res.send(404,''+error);
    }
  });
});

module.exports = router;