const router = require('express').Router();

const modelService = require('../services/model');
const jobService = require('../services/job');

router.post('/', function(req, res) {
    const sessionId = req.sessionID;
    // assume that job type is predictSingleLevel
    modelService.predictSingleLevel(sessionId);
    // whe have to keep a track (db) of the job
    jobId = 1;
    jsonResponse = {
      status: 'submitted',
      jobId: jobId
    }
    res.status('200').json(jsonResponse);
});

// this one has to be polled by the client
router.get('/:id', function(req, res) {
  // get back the jobId from the route
  // get the status of the job from db 'persistence layer'
  // the satus of the job will be handled by the service layer (model, predictSingleLevel)
  // const status = db.getJobStatus(jobId);
  const jobId = req.params.id;
  const status = jobService.getStatus(jobId);
  jsonResponse = {
    status
  };
  res.status('200').json(jsonResponse);
});

module.exports = router;