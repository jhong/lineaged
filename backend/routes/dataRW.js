const router = require('express').Router();
const path = require('path');


router.get('/assignment_ml.txt', function(req, res) {
  res.sendFile(path.resolve(__dirname + "/../dataRW/" + "assignment_ml.txt"))
});

module.exports = router;