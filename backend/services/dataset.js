const fs = require('fs');
const path = require('path');

const modelService = require('./model');

// TODO: rename as it does more than get parsed data, e.g setDefaults
// reads from path.join(__dirname, '../data/' + datasetName + '.json');
function getParsedData(datasetName, sessionId) {
    let filePath = path.join(__dirname, '../data/parsed' + datasetName + '.json');
    // TODO: async/await instead of callback on readFile
    fs.readFile(filePath, async (err, data) => {
        if (err) throw err;
        let cells = JSON.parse(data);
        let trianglesInfo = cells.trianglesInfo;
        let tissues = cells.tissues;
        let supporting = findSupportingCellsfromParsed(tissues, trianglesInfo);
        await modelService.setDefaults(sessionId, tissues, trianglesInfo, datasetName, supporting);
    });
}

// This function is to read the raw dataset (uploaded by the user)
// TODO: not used for now, and not working (following various refactor)
function readRawDatasets(fileName) {
    var filePath = path.join(__dirname, '../data/'+fileName+'.surf');
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
        var points = [];
        var triangles = [];
        var trianglesInfo = [];
        var tissues = [];
        if (!err) {
            var str = data.split(/\n/);
            var tempProperty = '';
            var flag = false;
  
            for (var i = 0; i < str.length; i++) {
                var singleString = str[i].trim().split(/\s+/);
                if (singleString[0] == 'Materials') {
                    flag = true;
                    continue;
                }
                if (flag == true && tempProperty == '' && singleString.length == 2 && singleString[1] == '{'){
                    tempProperty = singleString[0];
                    continue;
                }
                if (singleString[0] == 'Color') {
                    var singleTissue = [];
                    singleTissue.push(tempProperty);
                    singleTissue.push(parseFloat(singleString[1]));
                    singleTissue.push(parseFloat(singleString[2]));
                    singleTissue.push(parseFloat(singleString[3].split(',')[0]));
                    tissues.push(singleTissue);
                    continue;
                }
                if (singleString[0] == 'InnerRegion'){   var tempInner = singleString[1];   }
                if (singleString[0] == 'OuterRegion'){   var tempOuter = singleString[1];   }
                if (singleString[0] == 'Vertices' || singleString[0] == 'Triangles') {
                    tempProperty = singleString[0];
                    if (singleString[0] == 'Triangles'){
                        var singleTriangleInfo = [];
                        singleTriangleInfo.push(tempInner);
                        singleTriangleInfo.push(tempOuter);
                        singleTriangleInfo.push(parseInt(singleString[1]));
                        trianglesInfo.push(singleTriangleInfo);
                    }
                    continue;
                }    
                if ( (singleString == '}' || ( !(isNumeric(singleString[0])) && flag == false)) && tempProperty != ''){
                    tempProperty = '';
                    continue;
                }
                if (singleString == '}' && tempProperty == ''){ flag = false; }
                if (tempProperty == 'Vertices'){
                    var singlePoint = [];
                    singlePoint.push(parseFloat(singleString[0]));
                    singlePoint.push(parseFloat(singleString[1]));
                    singlePoint.push(parseFloat(singleString[2]));
                    points.push(singlePoint);
                }
                if (tempProperty == 'Triangles'){
                    var singleTriangle = [];
                    singleTriangle.push(parseInt(singleString[0]));
                    singleTriangle.push(parseInt(singleString[1]));
                    singleTriangle.push(parseInt(singleString[2]));
                    triangles.push(singleTriangle);
                }
            }
        } else {
            console.log(err);
        }
        var obj = {
            points: points,
            triangles: triangles,
            trianglesInfo: trianglesInfo,
            tissues: tissues
        };
        var json = JSON.stringify(obj, null, " ");
        fs.writeFile('./dataRW/parsedEmbryo.json', json, (err) => {
            if (err) throw err;
            console.log('Data written.');
        });
        let supporting = findSupportingCellsfromParsed(tissues, trianglesInfo);
        modelService.setDefaultLeftCells(tissues, trianglesInfo, fileName, supporting);
    });
}

function presentBasicLevel(tissues, trianglesInfo, supporting){
  var numList = numberExists(tissues);
  var max = Math.max(...numList);
  var assign = [];
  for (var i = 0; i < max; i++) {
      assign.push(0);
  }
  var names, neighbors;
  [names, neighbors] = getNamesForFirstGeneration(numList, trianglesInfo);
//   assignmentService.fromAssignFormJson(assign, numList, names, neighbors, trianglesInfo, tissues);
}

function numberExists(tissues) {
  var numList = [];
  for (var tissue of tissues) {
      numList.push(parseInt(tissue[0].replace( /^\D+/g,'')));
  }
  return numList.filter(function(d) {
      return !(isNaN(d));
  });
}

function getNamesForFirstGeneration(numList, trianglesInfo) {
  var names = [], neighbors = [];
  for (var i of numList) {
      names.push(i);
      neighbors.push([]);
      for (var j of trianglesInfo){
          var x = j[0].replace(/^\D+/g, "").replace(/^0+/,"");
          var y = j[1].replace(/^\D+/g, "").replace(/^0+/,"");
          if (x != '' && y != ''){
              if (parseInt(x) == i || parseInt(y) == i) {
                  neighbors[neighbors.length-1].push(trianglesInfo.indexOf(j));
              }
          }
          else {continue;}
      }
  }
  return [names, neighbors];
}

function isNumeric(str) {
  if (typeof str != "string") return false
  return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
         !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
}

function findSupportingCellsfromParsed(tissues, trianglesInfo) {
    let count = 0;
    let supporting = [];
    for (let j = 0; j < tissues.length; j++) {
        for (let i = 0; i < trianglesInfo.length; i++) {
            if (trianglesInfo[i][0] == tissues[j][0] || trianglesInfo[i][1] == tissues[j][0]) {
                count ++;
            }
        }
        if (count < 5) {supporting.push(tissues[j][0]);}
        count = 0;
    }
    return supporting;
}

module.exports = {
    getParsedData
};
