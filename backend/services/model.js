const tf = require("@tensorflow/tfjs");
const tfn = require('@tensorflow/tfjs-node');
const { progressBarHelper } = require("@tensorflow/tfjs-node/dist/callbacks");
const fs = require('fs');
const path = require('path');
const globalService = require('../services/global');
const building3dService = require('../services/3dbuilding');

// The json is filename.json formed by Python
function setDefaultLeftCellsFromPython(tissues, trianglesInfo, fileName) {
    const cp = require("child_process");
    const spawn = cp.spawn;
    const pyFile = path.join(__dirname,"../python/singleFileRead.py");
    const pyProg = spawn('python3', [pyFile, fileName, process.cwd()]);
    pyProg.stderr.pipe(process.stderr);
    pyProg.stdout.on('data', function(data) {
        console.log('Pipe data from Python script');
    })
    pyProg.stdout.on('end', (code) => {
        setDefaultLeftCells(tissues, trianglesInfo, fileName);
    });
}

async function setDefaults(sessionId, tissues, trianglesInfo, datasetName, supporting) {
    const globalLeftCells = [], 
          supportingCells = [];
    // generate supportingCells and globalLeftCells from tissues
    const regexpNum = /^\D+/g;
    for (let i = 1; i < tissues.length; i++) {
        if (supporting.includes(tissues[i][0])) {
            supportingCells.push(parseInt(tissues[i][0].replace(regexpNum, "")));
            continue;
        }
        globalLeftCells.push(parseInt(tissues[i][0].replace(regexpNum, "")));
    }
    const predictionConstrains = [globalLeftCells];
    const { globalBasicCells, building } = setDefaultBasicCells(tissues, trianglesInfo, datasetName, supportingCells);
    const globalCurrentAssignments = new Array(globalBasicCells[globalBasicCells.length-1]['name']).fill(0);
    const currentPairPossibilities = [];
    writeOriginalAssignmentFile(globalCurrentAssignments);
    await globalService.create(sessionId, globalLeftCells, globalCurrentAssignments, globalBasicCells, currentPairPossibilities, supportingCells, predictionConstrains);
    await building3dService.create(sessionId, building['trianglesArea'], building['tissues'], building['trianglesInfo']);
}

function setDefaultBasicCells(tissues, trianglesInfo, datasetName, supportingCells) {
    let jsonPath = path.join(__dirname, '../data/' + datasetName + '.json');
    // TODO: change because it is a blocking call
    const rawConfig = fs.readFileSync(jsonPath);
    const config = JSON.parse(rawConfig);
    let globalBasicCells = [], building = {};
    for (const configItem of config) {
        if (configItem.trianglesArea != undefined) {
            trianglesArea = configItem.trianglesArea;
            building['trianglesArea'] = trianglesArea;
            building['tissues'] = tissues;
            building['trianglesInfo'] = trianglesInfo;
        }
        if (supportingCells.includes(configItem.name)) {continue;}
        globalBasicCells.push({});
        globalBasicCells[globalBasicCells.length-1]['name'] = configItem.name;
        globalBasicCells[globalBasicCells.length-1]['index'] = configItem.index;
        globalBasicCells[globalBasicCells.length-1]['center'] = configItem.center;
        globalBasicCells[globalBasicCells.length-1]['neighbors'] = configItem.neighbor;
        globalBasicCells[globalBasicCells.length-1]['volume'] = configItem.volume;
        globalBasicCells[globalBasicCells.length-1]['surface'] = configItem.surface;
    }
    return {globalBasicCells, building};
}

// predicting all assignments
async function predictAll(sessionId) {
    const global = await globalService.get(sessionId);
    const globalLeftCells = global.leftCells;
    console.log("Predicting all.");   
    let layerCount =  parseInt(Math.log2(globalLeftCells.length))+1;
    for (var i = 1; i <= layerCount; i++) {
        predictSingleLevel(sessionId);
    }
}

// In the beginning, no assignments have been done, so we need to form a assignment_ml.txt file 
function writeOriginalAssignmentFile(currentAssignments) {
    var file = fs.createWriteStream(path.join(__dirname,"../dataRW/assignment_ml.txt"));
    file.on('error', function(err){throw err;})
    currentAssignments.forEach(function(i) {file.write(i+' ');});
    file.end();
}

async function predictSingleLevel(sessionId) {
    // will be in database after
    // we use temporarily dataRW, which is NOT the right place :)
    let statusFile = fs.createWriteStream(path.join(__dirname,"../dataRW/status.txt"));
    statusFile.write('InProgress');
    statusFile.close();
    var modelPath = tfn.io.fileSystem(path.join(__dirname,'../model/model.json'));
    const model = await tf.loadLayersModel(modelPath);
    console.log("Predicting a single level.");
    const global = await globalService.get(sessionId);
    const building = await building3dService.get(sessionId);

    const possibilities = levelTensors(model, global, building);
    // update the global document
    const { newLeftCells, newCurrentAssignments, newPairPossibilities } = fromPossibilityToParent(possibilities, global);
    global.leftCells = newLeftCells;
    global.currentAssignments = newCurrentAssignments;
    global.pairPossibilities = newPairPossibilities;

    // save modified global document to mongo
    await globalService.update(global);
    const file = fs.createWriteStream(path.join(__dirname,"../dataRW/assignment_ml.txt"));
    file.on('error', function(err) {console.log('error',err);});
    global.currentAssignments.forEach(function(data) {
        file.write(`${data} `);
    });
    file.end();
    fs.writeFileSync(path.join(__dirname,"../dataRW/status.txt"), 'Done');
}

function levelTensors(model, global, building){
    const globalLeftCells = global.leftCells,
          currentAssignments = global.currentAssignments,
          basicCells = global.basicCells,
          supportingCells = global.supportingCells;
          trianglesArea = building.trianglesArea,
          tissues = building.tissues,
          trianglesInfo = building.trianglesInfo;
    var centersLevel = [],
        distances = [],
        neighborsLevel = [],
        volumesLevel = [],
        surfacesLevel = [];
    for (var i of globalLeftCells) {
        var leaves = findLeaves(i, currentAssignments, basicCells);
        centersLevel.push(calculateCenters(leaves, basicCells));
        neighborsLevel.push(getLevelNeighbors(leaves, i, globalLeftCells, currentAssignments, basicCells, supportingCells));
        volumesLevel.push(getLevelVolumes(leaves, basicCells));
        surfacesLevel.push(getLevelSurfaces(leaves, basicCells, trianglesArea, tissues, trianglesInfo));
    }
    // find all pairs & calculate the distance
    for (var i of globalLeftCells) {
        var indexI = globalLeftCells.indexOf(i);
        for (var j of neighborsLevel[indexI]) {
            if (j <= i) {continue;}
            var indexJ = globalLeftCells.indexOf(j);
            distances.push(getDistance(centersLevel[indexI], centersLevel[indexJ]));
        }
    }
    var distancesNor = normalizationArr(distances);
    var volumesNor = normalizationArr(volumesLevel);
    var volumesLevel1 = volumesLevel.slice();
    volumesLevel1 = volumesLevel1.filter(i => i!=0);
    var volumesMin = Math.min(...volumesLevel1);
    var surfacesNor = normalizationArr(surfacesLevel);
    var surfaceMax = Math.max(...surfacesLevel);
    var surfacesLevel1 = surfacesLevel.slice();
    surfacesLevel1 = surfacesLevel1.filter(i => i!=0);
    var surfaceMin = Math.min(...surfacesLevel1);

    var count = 0,
        possibilities = [];
    for (var i of globalLeftCells) {
        var indexI = globalLeftCells.indexOf(i);
        for (var j of neighborsLevel[indexI]) {
            if (j < i) {continue;}
            var tensor = [];
            var indexJ = globalLeftCells.indexOf(j);
            tensor.push(distancesNor[count]);
            tensor.push(neighborsLevel[indexI].length);
            tensor.push(neighborsLevel[indexJ].length);
            tensor.push(volumesNor[indexI]);
            tensor.push(volumesNor[indexJ]);
            tensor.push(surfacesNor[indexI]);
            tensor.push(surfacesNor[indexJ]);
            tensor.push(normalize(findSharedArea(i,j, currentAssignments, basicCells, trianglesArea, trianglesInfo, tissues), surfaceMax, surfaceMin));
            var posi = model.predict(tf.tensor2d([tensor]));
            possibilities.push([i,j,posi.dataSync()]);
            count ++;
        }
    }
    return possibilities;
}

function fromPossibilityToParent(possibilities, global) {
    // clone to avoid mutation of global object
    const newLeftCells = global.leftCells.slice();
    const newCurrentAssignments = global.currentAssignments.slice();
    const newPairPossibilities = global.pairPossibilities.slice();
    const basicCells = global.basicCells.slice();

    possibilities.sort((a,b) => { return b[2] - a[2]; });

    for (let possibility of possibilities) {
        // only record pairs with more than 0.5 possibilities
        if (possibility[2] > 0.5 && newLeftCells.includes(possibility[0]) && 
        newLeftCells.includes(possibility[1]) && checkTwoValuesInSameArray(possibility[0],possibility[1],global.predictionConstrains, newCurrentAssignments, basicCells)) {
            newPairPossibilities.push({});
            newPairPossibilities[newPairPossibilities.length-1]['pairs'] = [possibility[0], possibility[1]];
            newPairPossibilities[newPairPossibilities.length-1]['possibility'] = possibility[2];

            //TODO: cache length
            newCurrentAssignments[possibility[0]-1] = newCurrentAssignments.length + 1;
            newCurrentAssignments[possibility[1]-1] = newCurrentAssignments.length + 1;
            newCurrentAssignments.push(0);

            newLeftCells.splice(newLeftCells.indexOf(possibility[0]), 1);
            newLeftCells.splice(newLeftCells.indexOf(possibility[1]), 1);
            newLeftCells.push(newCurrentAssignments.length);
        }
    }

    return {
        newLeftCells,
        newCurrentAssignments,
        newPairPossibilities
    }
}

// Train the model
async function trainModel(newPair, possibility)
{
  const modelPath = tfn.io.fileSystem('model_retrained');
  var model;
  try{
       model = await tf.loadLayersModel(modelPath+'/model.json');    
  }
  catch(error){
      console.log('using original model as base');
      const originalModelPath = tfn.io.fileSystem('model/model.json');
      model = await tf.loadLayersModel(originalModelPath);
  }
  
  model.compile({loss:'binaryCrossentropy',optimizer:'adam',metrics:['accuracy']});
  const history = await model.fit(tf.tensor2d([newPair]), tf.tensor2d([[possibility]]), {
     batchSize: 1,
     epochs: 4
  });
  model.save(modelPath);
}

function findLeaves(name, currentAssignments, basicCells) {
    if (name <= basicCells[basicCells.length-1]['name']) {return [name];}
    else {
        var l = [];
        for (var i = 0; i < currentAssignments.length; i++){
            if (currentAssignments[i] == name) {
                l.push(i+1);
            }
            if (l.length == 2) {return findLeaves(l[0], currentAssignments, basicCells).concat(findLeaves(l[1], currentAssignments, basicCells));}
        }
    }
}

function calculateCenters(leaves, basicCells) {
  var x = 0, y = 0, z = 0;
  var volume = 0;
  for (var i of leaves) {
      var index = searchIndexByName(i, basicCells);
      var tempVolume = basicCells[index]['volume'];
      volume += tempVolume;
      x += basicCells[index]['center'][0]*tempVolume;
      y += basicCells[index]['center'][1]*tempVolume;
      z += basicCells[index]['center'][2]*tempVolume;
  }
  return [x/volume, y/volume, z/volume];
}

function searchIndexByName(name, basicCells) {
  for (var i = 0; i < basicCells.length; i++) {
      if (basicCells[i]['name'] == name) {
          return i;
      }
  }
}

function getLevelNeighbors(leaves, name, globalLeftCells, currentAssignments, basicCells, supportingCells) {
  var allNeighbors = [];
  var neighborsInLevel = [];
  for (var leaf of leaves) {
      var index = searchIndexByName(leaf, basicCells);
      allNeighbors = allNeighbors.concat(basicCells[index]['neighbors']);
  }
  var setNeighbors = [...new Set(allNeighbors)];
  for (const i of setNeighbors) {
      if (supportingCells.includes(i)) {continue;}
      var parentInLevel = i;
      while (!globalLeftCells.includes(parentInLevel)){
          parentInLevel = currentAssignments[parentInLevel-1];
      }
      if (parentInLevel != name) {
          neighborsInLevel.push(parentInLevel);
      }
  }
  return [... new Set(neighborsInLevel)];
}

function getLevelVolumes(leaves, basicCells) {
  var volumeInLevel = 0;
  for (var leaf of leaves) {
      var index = searchIndexByName(leaf, basicCells);
      volumeInLevel += basicCells[index]['volume'];
  }
  return volumeInLevel;
}

function getLevelSurfaces(leaves, basicCells, trianglesArea, tissues, trianglesInfo) {
  var surfaceInLevel = 0;
  var trianglesInLevel = findParentTriangles(leaves, basicCells, tissues, trianglesInfo);
  for (var tri of trianglesInLevel) {
      surfaceInLevel += trianglesArea[tri];
  }
  return surfaceInLevel;
}

function findParentTriangles(leaves, basicCells, tissues, trianglesInfo) {
    var trianglesList = [];
    for (var leaf of leaves) {
        var index = basicCells[searchIndexByName(leaf, basicCells)]['index'];
        trianglesList = addTwoArray(trianglesList, MLfindTriangle(index, tissues, trianglesInfo));
    }
    return trianglesList;
}

function MLfindTriangle(tissueIndex, tissues, trianglesInfo) {
  var triangleList = [];
  for (var i = 1; i < trianglesInfo.length; i++) {
      if (trianglesInfo[i][0] == tissues[tissueIndex][0] ||
      trianglesInfo[i][1] == tissues[tissueIndex][0]) {
              triangleList.push(i);
          }
  }
  return triangleList;
}

// delete the same elements
function addTwoArray(array1, array2) {
  for (var i of array2){
      if (!array1.includes(i)) {
          array1.push(i);
      }
      else {
          array1.splice(array1.indexOf(i),1);
      }
  }
  return array1;
}

function normalizationArr(array) {
  var normArray = [];
  var max = 0;
  var min = 20000;
  for (var i = 0; i < array.length; i++) {
      if (array[i] > max)    {max = array[i];}
      if (array[i] < min && array[i] != 0)    {min = array[i];}
  }
  for (var i = 0; i < array.length; i++) {
      var norm = (array[i]-min)/(max-min);
      normArray.push(norm);
  }
  return normArray;
}

function tissueFindTriangle(name, child, numList, order, trianglesInfo, tissues) {
  var trianglesList = [];
  if (name <= numList[numList.length-1]) {
      var tissueIndex = numList.indexOf(name)+1;
      for (var i = 0; i < trianglesInfo.length; i ++) {
          if (trianglesInfo[i][0] == tissues[tissueIndex][0] || 
          trianglesInfo[i][1] == tissues[tissueIndex][0]) {
                  trianglesList.push(i);
              }
      }
  }
  else {
      var children = child[order.indexOf(name)];
      if (children.length <= 1) {console.log(name, children);}
      var child1 = tissueFindTriangle(children[0], child),
          child2 = tissueFindTriangle(children[1], child);
      trianglesList = addTwoArray(child1, child2);
  }
  return trianglesList;
}

function getVolumes(trianglesList, trianglesInfo){
  var volume = 0;
  for (var i of trianglesList) {
      volume += trianglesInfo[i][2];
  }
  return volume;
}

function getDistance(position1, position2){
    x = position1[0] - position2[0];
    y = position1[1] - position2[1];
    z = position1[2] - position2[2];
    return Math.sqrt(x**2 + y**2 + z**2);
}

function normalize(value, min, max) {
    if (min == undefined || max == undefined) {return value;}
    else {return (value-min)/(max-min);}
}

function findSharedArea(cell1, cell2, currentAssignments, basicCells, trianglesArea, trianglesInfo, tissues){
    var sharedArea = 0;
    if (cell1 != cell2){
        var leaves1 = findLeaves(cell1, currentAssignments, basicCells),
            leaves2 = findLeaves(cell2, currentAssignments, basicCells);
        var array1 = findParentTriangles(leaves1, basicCells, trianglesInfo, tissues),
            array2 = findParentTriangles(leaves2, basicCells, trianglesInfo, tissues);
        var intersect = array1.filter(value => array2.includes(value));
    }
    else {
        return 0;
        // var intersect = MLfindTriangle(cell1);
    }
    for (var i of intersect){
        sharedArea += trianglesArea[i];
    }
    return sharedArea;
}

function findSurfaceConnection(cell1, cell1Neighbors, cell2, namesOrder, centersOrder){
    if (!cell1Neighbors.includes(namesOrder[cell2])){
        return 0;
    }
    var connects = 1;
    var v1 = pointsVector(centersOrder[cell1], centersOrder[cell2]);
    for (var i of cell1Neighbors) {
        if (i == namesOrder[cell2]) {continue;}
        var v2 = pointsVector(centersOrder[cell1], centersOrder[cell2]);
        var angle = Math.atan2(v2[1]-v1[1], v2[0]-v1[0])*180/Math.PI;
        if (angle <= angle) {
            connects += 1;
        }
    }
    return connects;
}

async function getPairPossibilities(sessionId) {
    const global = await globalService.get(sessionId);
    return global.pairPossibilities;
}

function checkTwoValuesInSameArray(value1, value2, array2d, assignments, basicCells) {
    const leaves1 = findLeaves(value1, assignments, basicCells);
    const leaves2 = findLeaves(value2, assignments, basicCells);
    for (let array of array2d) {
        if (checkIntersected(array, leaves1) && checkIntersected(array, leaves2)) {
            return true;
        }
    }
    return false;
}

function checkIntersected(array1, array2) {
    for (let element of array1) {
        if (array2.includes(element)){return true;}
    }
    return false;
}

module.exports = {
    setDefaults,
    predictAll,
    predictSingleLevel,
    trainModel,
    getPairPossibilities
}