const fs = require('fs');
const path = require('path');

function getStatus(jobId) {
  const statusFile = path.join(__dirname, '../dataRW/status.txt');
  let status = '';
  // TODO: change because it is a blocking call
  status = fs.readFileSync(statusFile, {encoding: 'utf-8'});
  console.log(status);
  return status;
}

module.exports = {
  getStatus
}