const fs = require('fs');

function fromAssignFormJson(assign, numList, names, neighbors, trianglesInfo, tissues) {
  var cells = [],
      depths = [],
      children = [],
      sister = [],
      parent = [],
      volumes = [];

  var supporting = findSupportingCell(numList, names, neighbors);
  var order = getCellOrder(assign.length, assign, numList);
  order = deleteSupporting(order, supporting, assign);

  for (var i of order){
      var num = parseInt(assign[i-1]);
      // if the num is new formed or exists already
      if (i > numList[numList.length-1] || numList.includes(i)){
          // var name = "Cell" + i;
          var name = i;
          cells.push(name);
          // the first level
          if (numList.includes(i)){
              depths.push([1]);
              children.push(0);
              parent.push(parseInt(assign[i-1]));                        
          }
          // Higher level
          else {
              var child = [];
              var temp_depth = 0;
              for (var j = 1; j < i; j++){
                  if (child.length == 2) {break;}
                  if (parseInt(assign[j-1]) == i) {
                      child.push(j);
                      var temp_max = Math.max(...depths[cells.indexOf(j)]);
                      if (numList.includes(j) && temp_depth <= 2){
                          temp_depth = 2;
                      }
                      else if ( temp_max >= temp_depth){
                          temp_depth = parseInt(temp_max)+1;
                      }
                  }
              }
              depths.push([temp_depth]);
              children.push(child);
              parent.push(parseInt(assign[i-1]));
          }
          for (var j = 0; j < assign.length; j++) {
              if (assign[i-1] == 0) {
                  sister.push(0);
                  break;
              }
              if (j != i-1 && assign[j] != 0 && assign[j] == assign[i-1]){
                  sister.push(j+1);
                  break;
              }
          }
      }
  }
  // console.log(children.length);
  for (var i of order){
      var triangles = tissueFindTriangle(i, children, numList, order, trianglesInfo, tissues);
      if (triangles.length == 0) {
          var index = cells.indexOf(i);
          // console.log(i, index);
          cells.splice(index,1);
          depths.splice(index,1);
          children.splice(index,1);
          sister.splice(index,1);
          parent.splice(index,1);
          continue;
      }
      volumes.push(getVolumes(triangles, trianglesInfo));
  }

  // some cells have two depth
  for (var i = 0; i < cells.length; i++) {
      if (sister[i] == 0) {continue;}
      var depth_sister = depths[cells.indexOf(sister[i])][0];
      if (depths[i][0] < depth_sister) {
          for (var j = depths[i][0]+1; j <= depth_sister; j++){
              depths[i].push(j);
          }
      }
  }
  saveAssignmentJson(cells, depths, children, sister, parent, volumes);
}

function saveAssignmentJson(cells, depths, children, sister, parent, volumes) {
  let result = [];
  for (var i = 0; i < cells.length; i++) {
      let obj = {};
      obj.name = cells[i];
      obj.depth = depths[i];
      obj.children = children[i];
      obj.sister = sister[i];
      obj.parent = parent[i];
      obj.volume = volumes[i];
      result.push(obj);
  }
  fs.writeFile('./dataRW/assignment.json', JSON.stringify(result, null, " "),(err) => {
      if (err) throw err;
  });
}

function findSupportingCell(numList, names, neighbors) {
  var supporting = [];
  for (var i = 0; i < numList.length; i++) {
      var tmp = numList[i];
      if (neighbors[names.indexOf(tmp)].length <= 5) {
          supporting.push(tmp);
      }
  }
  return supporting;
}

function getCellOrder(name, assign, numList) {
  var order = [];
  for (var i = name; i > 0; i--) {
      if (!order.includes(i) && (assign.includes(i.toString()) || name <= numList[numList.length-1])) {
          order = postCellOrder(i, assign, order);
      }
  }
  return order;
}

function postCellOrder(name, assign, order) {
  if (!assign.includes(name.toString())) {order.push(name);return order;}
  var child = [];
  for (var i = name-1; i > 0; i--) {
      if (assign[i-1] == name) {child.push(i);}
      if (child.length == 2) {break;}
  }
  postCellOrder(child[0], assign, order);
  postCellOrder(child[1], assign, order);
  order.push(name);
  return order;
}

function deleteSupporting(order, supporting, assign) {
  var indexes = [];
  for (var i of order) {
      if (supporting.includes(i)) {
          indexes.push(order.indexOf(i));
      }
  }
  for (var i of supporting) {
      if (assign[i-1] != 0) {
          indexes.push(order.indexOf(parseInt(assign[i-1])));
      }
  }
  var indexesNew = [...new Set(indexes)];
  indexesNew.sort(function(a,b) {return b-a;});
  for (var i of indexesNew) {
      order.splice(i, 1);
  }
  return order;
}

function tissueFindTriangle(name, child, numList, order, trianglesInfo, tissues) {
    var trianglesList = [];
    // console.log(numList.length, numList[numList.length-1]);
    if (name <= numList[numList.length-1]) {
        var tissueIndex = numList.indexOf(name)+1;
        for (var i = 0; i < trianglesInfo.length; i ++) {
            if (trianglesInfo[i][0] == tissues[tissueIndex][0] || 
            trianglesInfo[i][1] == tissues[tissueIndex][0]) {
                    trianglesList.push(i);
                }
        }
    }
    else {
        // console.log(name);
        var children = child[order.indexOf(name)];
        if (children.length <= 1) {console.log(name, children);}
        var child1 = tissueFindTriangle(children[0], child),
            child2 = tissueFindTriangle(children[1], child);
        trianglesList = addTwoArray(child1, child2);
    }
    return trianglesList;
}

function getVolumes(trianglesList, trianglesInfo){
    var volume = 0;
    for (var i of trianglesList) {
        volume += trianglesInfo[i][2];
    }
    return volume;
}

module.exports = {
    fromAssignFormJson
  };