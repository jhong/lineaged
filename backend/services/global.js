const globalModel = require('../models/global');

async function create(sessionId, leftCells, currentAssignments, basicCells, pairPossibilities, supportingCells, predictionConstrains) {
    // first ensure all data with the existing session is cleared
    await deleteAll(sessionId);

    // we can create a fresh entry
    const newGlobalModel = new globalModel({
        sessionId,
        leftCells,
        currentAssignments,
        basicCells,
        pairPossibilities,
        supportingCells,
        predictionConstrains
    });
  try {
        await newGlobalModel.save();
  } catch (e) {
      throw Error('Error while Creating Global: ' + e);
  }
}

async function deleteAll(sessionId) {
    try {
        await globalModel.deleteMany({
            sessionId: sessionId
        });
    } catch (e) {
        throw Error('Error while deleting Global: ' + e);
    }
}

async function get(sessionId) {
    let doc = {};
    try {
        doc = await globalModel.findOne({
            sessionId: sessionId
        })
        // exec gives better stack traces with await (exec location in code)
        .exec();
    } catch (e) {
        throw Error('Error while getting Global document: ' + e);
    }
    return doc;
}

async function update(doc) {
    try {
        await doc.save();
    } catch (e) {
        throw Error('Error while updating Global document: ' + e);
    }
}

async function newAssignmentsUpdate(sessionId, existingCells, currentAssignments) {
    try{
        // assume that job type is predictSingleLevel
        const global = await get(sessionId);
        let lastCell = Math.max(...currentAssignments);
        let newCurrentAssignments = currentAssignments.slice();
        let newLeftCells = [];
        for (let cell = 1; cell <= lastCell; cell++) {
            if (existingCells.includes(cell)) {
                let index = existingCells.indexOf(cell);
                if (currentAssignments[index] == 0) {
                    newLeftCells.push(cell);
                }
            } else if (cell > existingCells[existingCells.length-1]) {
                newLeftCells.push(cell);
            }
            else {
                newCurrentAssignments.splice(cell-1 ,0, 0);
            }
        }
        global.leftCells = newLeftCells;
        global.currentAssignments = newCurrentAssignments;
        update(global);
    } catch (e) {
        throw Error('Error while updating new assignments: ' + e);
    }
}

async function supportingUpdate(sessionID, supportingCells) {
    try{
        // assume that job type is predictSingleLevel
        const global = await get(sessionID);
        let newSupportingCells = [];
        let newLeftCells = global.leftCells.slice();
        if (global.supportingCells.length == 0) {
            newSupportingCells = supportingCells;
        } else {
            let oldSupportingCells = global.supportingCells.slice();
            newSupportingCells = [...new Set(oldSupportingCells.concat(supportingCells))];
        }
        for (let cell of newSupportingCells) {
            if (newLeftCells.includes(cell)) {
                const index = newLeftCells.indexOf(cell);
                newLeftCells.splice(index, 1);
            }
        }
        global.leftCells = newLeftCells;
        global.supportingCells = newSupportingCells;
        console.log(newLeftCells, newSupportingCells);
        update(global);
    } catch (e) {
        throw Error('Error while updating new assignments: ' + e);
    }
}

async function predictionConstrainsUpdate(sessionID, predictionConstrains) {
    try{
        const global = await get(sessionID);
        let constrains = predictionConstrains;
        global.predictionConstrains = constrains;
        console.log(global.predictionConstrains);
        update(global);
    } catch (e) {
        throw Error('Error while constraining predictions: ' + e);
    }
}

module.exports = {
    create,
    get,
    update,
    newAssignmentsUpdate,
    supportingUpdate,
    predictionConstrainsUpdate
}