const buildingModel = require('../models/3dbuilding');

async function create(sessionId, trianglesArea, tissues, trianglesInfo) {
    await deleteAll(sessionId);
    const newBuildingModel = new buildingModel({
        sessionId,
        trianglesArea,
        tissues,
        trianglesInfo
    });

  try {
      // Saving the Counter 
      await newBuildingModel.save();
  } catch (e) {
      throw Error('Error while Creating Building: ' + e);
  }
}

async function deleteAll(sessionId) {
    try {
        await buildingModel.deleteMany({
            sessionId: sessionId
        });
    } catch (e) {
        throw Error('Error while deleting Building: ' + e);
    }
}


async function get(sessionId) {
    let doc = {};
    try {
        doc = await buildingModel.findOne({
            sessionId: sessionId
        })
        // exec gives better stack traces with await (exec location in code)
        .exec();
    } catch (e) {
        throw Error('Error while getting Building document: ' + e);
    }
    return doc;
}


async function update(doc) {
    try {
        await doc.save();
    } catch (e) {
        throw Error('Error while updating Building document: ' + e);
    }
}


module.exports = {
    create,
    get,
    update
}