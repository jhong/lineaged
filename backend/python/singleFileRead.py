import sys
import os
import vtk
import numpy as np
import json
import glob
import time
import math

class Data:
    def __init__(self, points, triangles, trianglesInfo, cells):
        self.points = points
        self.triangles = triangles
        self.trianglesInfo = trianglesInfo
        self.cells = cells

class Cell:
    def __init__(self, name, Id):
        self.Id = Id
        self.name = name
        self.neighborCount = 0

    def setNeighborCount(self, count):
        self.neighborCount = count
    

# cell number, cell center position, cell layer, 
# top counts, bottom counts, left counts, right counts, front counts, back counts, 
# top area, bottom area, left area, right area, front area, back area

def createFile(jsonFile):
    if not os.path.exists(jsonFile):
        with open(jsonFile,'w'): pass
    with open(jsonFile, mode = 'w',  encoding='utf-8') as file:
        json.dump([], file)
        # writer = csv.writer(file)
        # objectConnects is the counts of objects shared with that surface
        # writer.writerow(["cell1", "cell2", "position1", "position2", "sharedArea", "objectConnects1", "objectConnects2", "Possibility"])

def loadSurfaceData(surfaceFileName):
    cells = []
    points = []
    triangles = []
    trianglesInfo = []

    try:
        with open(surfaceFileName) as sfp:
            tempProperty = ''
            flag = False
            if ( sfp.readline() != "# HyperSurface 0.1 ASCII\n"):
                print("Wrong File Format")
                return None

            for line in sfp:
                line = line.strip()
                # print(flag)
                if ( line == ''):    continue 
                if ( line.split()[0] == 'Materials'): 
                    flag = True
                    continue
                if ( flag == True and tempProperty == '' and len(line.split()) == 2 and line.split()[1] =='{'):
                    tempProperty = line.split()[0]
                    continue
                if ( line.split()[0] == 'Id'):
                    cells.append( Cell(tempProperty, get_digits(line.split()[1])) )
                    continue
                if ( line.split()[0] == 'InnerRegion'): tempInner = line.split()[1]
                if ( line.split()[0] == 'OuterRegion'): tempOuter = line.split()[1]

                if ( line.split()[0] == 'Vertices' or line.split()[0] == 'Triangles' ):
                    tempProperty = line.split()[0]
                    if (line.split()[0] == 'Triangles'):
                        ## inner tissue, outer tissue, amount of triangles
                        trianglesInfo.append( (tempInner, tempOuter, int(line.split()[1])) )
                    continue

                if ( (line == '}' or (not(is_number(line.split()[0])) and flag == False))
                    and tempProperty != '' ) :  
                    tempProperty = ''
                    continue
                # decide whether the materials have been recorded yet
                if ( line == '}' and tempProperty == ''): flag = False

                if ( tempProperty == 'Vertices' ):
                    points.append( (float(line.split()[0]), float(line.split()[1]), float(line.split()[2])) )
                    # if ( len(points) == 1 ) :
                    #     # xmin, xmax, ymin, ymax, zmin, zmax
                    #     dataRange = [ points[0][0], points[0][0], points[0][1], points[0][1], points[0][2],points[0][2] ]
                    # else:
                    #     dataRange = updateRange(dataRange, points[-1])

                if ( tempProperty == 'Triangles' ):
                    ## Record all the triangles x y z 
                    triangles.append( (int(line.split()[0]), int(line.split()[1]), int(line.split()[2])) )

            # center = [(dataRange[0]+dataRange[1])/2, (dataRange[2]+dataRange[3])/2, (dataRange[4]+dataRange[5])/2]

    except IOError:
        return None            
            
    return Data(points, triangles, trianglesInfo, cells)

def is_number(string):
    try:
        float(string)
        return True

    except ValueError:
        return False

# def tellSurfaceCounts(data):


# def sharedArea(cell1, cell2, trianglesArea):
#     area = 0

# x, y, z, layer is delta x, y, z, layer of center1 and center2 (cell1 - cell2)
def writeJson(fileName, distance, neighborCount1, neighborCount2, volume1, volume2, surface1, surface2, sharedArea, objectConnects1, objectConnects2):
    with open(fileName, mode = 'r', encoding='utf-8') as file:
        cells = json.load(file)
    with open(fileName, mode = 'w', encoding='utf-8') as file:
        entry = {'distance': distance,
                 'neighborCount1': neighborCount1,
                 'neighborCount2': neighborCount2,
                 'volume1': volume1,
                 'volume2': volume2,
                 'surface1': surface1,
                 'surface2': surface2,
                 'sharedArea': sharedArea,
                 'objectConnects1': objectConnects1,
                 'objectConnects2': objectConnects2,
                 }
        cells.append(entry)
        json.dump(cells, file, indent = 2)
        # writer = csv.writer(file)
        # writer.writerow([cell1, cell2, center1, center2, sharedArea, objectConnects1, objectConnects2, possibility])


def calTriangleArea(data):
    trianglesArea = []
    startPoint = []
    for i in range(len(data.trianglesInfo)):
        if (i == 0):    startPoint = [0]
        else:   startPoint.append(startPoint[-1] + data.trianglesInfo[i-1][2])

        area = 0
        for j in range(startPoint[i], startPoint[i] + data.trianglesInfo[i][2]):
            area += vtk.vtkTriangle().TriangleArea(data.points[data.triangles[j][0]-1], data.points[data.triangles[j][1]-1], data.points[data.triangles[j][2]-1])
        trianglesArea.append(area)
    return startPoint, trianglesArea


# calculate the actor center and volume
def calPosition(startPoint, points, data):
    centers = []
    volumes = []
    surfaces = []
    for i in range(len(data.cells)):
        triangles = tissueFindTriangle(i, data.cells, data.trianglesInfo)
        if (len(triangles) == 0):
            centers.append((0,0,0))
            volumes.append(0)
            surfaces.append(0)
            continue
        actor = trianglesFormActor(startPoint, points, triangles, data)
        center = actor.GetCenter()
        centers.append(center)

        # cal the volume
        appendFilter = vtk.vtkAppendPolyData()
        appendFilter.AddInputData(actor.GetMapper().GetInput())
        appendFilter.Update()
        inputData = vtk.vtkMassProperties()
        inputData.SetInputData(appendFilter.GetOutput())
        inputData.Update()

        volumes.append(inputData.GetVolume())
        surfaces.append(inputData.GetSurfaceArea())
        
    return centers, volumes, surfaces

# Find which triangles belong to a specific tissue
def tissueFindTriangle(tissueIndex, cells, trianglesInfo):
    # return triangle index list
    triangleList = []
    for i in range(len(trianglesInfo)):
        if (trianglesInfo[i][0] == cells[tissueIndex].name or
            trianglesInfo[i][1] == cells[tissueIndex].name):
            triangleList.append(i)

    return triangleList

# Find shared area or surface area of the single cell
def findSharedArea(cell1, cell2, cells, trianglesArea, trianglesInfo):
    sharedArea = 0
    if (cell1 != cell2):
        intersect = list(set(tissueFindTriangle(cell1, cells, trianglesInfo)).intersection(tissueFindTriangle(cell2, cells, trianglesInfo)))
    else: interset = tissueFindTriangle(cell1, cells, trianglesInfo)
    for i in intersect:
        sharedArea += trianglesArea[i]
    return sharedArea


def trianglesFormActor(startPoint, points, triangles, data):
    model = vtk.vtkPolyData()
    polys = vtk.vtkCellArray()
    model.SetPoints(points)

    for j in triangles:
        for k in range(startPoint[j], startPoint[j]+ data.trianglesInfo[j][2]):
            polys.InsertNextCell(mkVTKIdList(data.triangles[k]))
            model.SetPolys(polys)
    del polys

    modelMapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:  modelMapper.SetInput(model)
    else:   modelMapper.SetInputData(model)
    del model

    modelActor = vtk.vtkActor()
    modelActor.SetMapper(modelMapper)
    # print(modelActor.GetProperty().Get)
    del modelMapper

    return modelActor

def mkVTKIdList(it):
    vil = vtk.vtkIdList()
    for i in it:
        vil.InsertNextId(int(i)-1)
    return vil

def getIdFromName(cells, name):
    for cell in cells:
        if (cell.name == name):
            return cell.Id
    return -1

def findSurfaceConnection(cell1, cell1_neighbors, cell2, centers):
    if (cell2 not in cell1_neighbors):
        return 0
    connects = 1
    v1 = pointsVector(centers[cell1], centers[cell2])
    for i in cell1_neighbors:
        # print(i)
        if (i == cell2):    continue
        v2 = pointsVector(centers[cell1], centers[i])
        angle = angle_between(v1, v2)
        # print(cell1, cell2, i, angle)
        if (angle <= 45):
            connects += 1
    return connects


def pointsVector(p1, p2):
    return (p1[0]-p2[0], p1[1]-p2[1], p1[2]-p2[2])

def angle_between(v1, v2):
    v1_u = v1 / np.linalg.norm(v1)
    v2_u = v2 / np.linalg.norm(v2)
    angle = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)) * 360
    while(angle >= 360):
        angle -= 360
    while(angle < 0):
        angle += 360
    return angle


def findNeighbors(cellIndex, tissues, trianglesInfo):
    findedTissuesName = []
    findedTissuesList = []
    for i in range(len(trianglesInfo)):
        if (trianglesInfo[i][0] == tissues[cellIndex].name and
            trianglesInfo[i][1] != 'Exterior'):
            findedTissuesName.append(trianglesInfo[i][1])
        elif (trianglesInfo[i][1] == tissues[cellIndex].name):
            findedTissuesName.append(trianglesInfo[i][0])
    for i in range(len(tissues)):
        if (tissues[i].name in findedTissuesName):
            if (tissues[i].name == 'Exterior'): continue
            findedTissuesList.append(int(tissues[i].name[-3:]))
    return list(set(findedTissuesList))


def readAssignment(assignmentFile):
    assignments = []
    try:
        with open(assignmentFile) as afp:
            for line in afp:
                line = line.strip()
                assignments.extend(line.split())
        assignments = list(map(int, assignments))
        # print(assignments)
    except IOError:
        return None  
    return assignments

def get_digits(str1):
    c = ""
    for i in str1:
        if i.isdigit():
            c += i
    return int(c)

def get_distance(position1, position2):
    x = position1[0] - position2[0]
    y = position1[1] - position2[1]
    z = position1[2] - position2[2]
    return math.sqrt(x**2 + y**2 + z**2)



def main():
    fileName = sys.argv[1]
    jsonFile = os.path.join(sys.argv[2],"data", "{}.json".format(fileName))
    createFile(jsonFile)
    file = os.path.join(sys.argv[2], "data", "{}.surf".format(fileName))    
    data = loadSurfaceData(file)
    startPoint, trianglesArea = calTriangleArea(data)
    points = vtk.vtkPoints()
    for i in range(len(data.points)):
        points.InsertPoint(i, data.points[i])
    centers, volumes, surfaces = calPosition(startPoint, points, data)
    neighbors = []
    for i in range(len(data.cells)):
        neighbors.append(findNeighbors(i, data.cells, data.trianglesInfo))

    for i in range(len(data.cells)):
        if (data.cells[i].name == 'Exterior'): continue
        with open(jsonFile, mode = 'r', encoding='utf-8') as file:
            cells = json.load(file)
        with open(jsonFile, mode = 'w', encoding='utf-8') as file:
            if (i == 1):
                entry = {
                    'name': int(data.cells[i].name[-3:]),
                    'index':i,
                    'center': centers[i],
                    'neighbor': neighbors[i],
                    'volume': volumes[i],
                    'surface': surfaces[i],
                    'trianglesArea': trianglesArea
                    }
            else:
                entry = {'name': int(data.cells[i].name[-3:]),
                    'index':i,
                    'center': centers[i],
                    'neighbor': neighbors[i],
                    'volume': volumes[i],
                    'surface': surfaces[i],
                    }
            cells.append(entry)
            json.dump(cells, file, indent = 2)
    # print(centers, neighbors, volumes, surfaces, trianglesArea)
    # print(centers)
    # sys.stdout.flush()
    # for i in range(len(data.cells)):
    #     for j in neighbors[i]:
    #         if (j <= i or i == 0): continue
    #         if (data.cells[i].name == 'Exterior' or data.cells[j].name == 'Exterior'):  continue
    #         cell1 = get_digits(data.cells[i].name)
    #         cell2 = get_digits(data.cells[j].name)
    #         # if (cell1 > max(assignment) or cell2 > max(assignment)):    continue
    #         sharedArea = findSharedArea(i, j, data.cells, trianglesArea, data.trianglesInfo)

    #         objectConnects1 = findSurfaceConnection(i, neighbors[i], j, centers)
    #         objectConnects2 = findSurfaceConnection(j, neighbors[j], i, centers)

    #         writeJson(jsonFile, get_distance(centers[i], centers[j]), len(neighbors[i]), len(neighbors[j]), volumes[i], volumes[j], surfaces[i], surfaces[j], sharedArea, objectConnects1, objectConnects2)


if __name__ == '__main__':
    main()



