
// mongoose is already set up in app.js
// and available here as some kind of singleton
const mongoose = require('mongoose');

//TODO: add the sessionID
const buildingSchema = new mongoose.Schema({
  // for us number of the cells are their name
  sessionId: String,
  trianglesArea: [Number],
  tissues: [[mongoose.Schema.Types.Mixed]],
  trianglesInfo: [[mongoose.Schema.Types.Mixed]]
}, {
  // let mongoose schema handle createdAt and updatedAt
  timestamps: true
});

// Compile mode from schema and export it
module.exports = mongoose.model('buildingModel', buildingSchema);