// global_left_cell schema

/**
 * global_left_cells: array of number; every element is an integer representing the names. e.g. [1,2,3,4,5,6,7]
global_basic_cells: array of object; every cell’s properties, including cell name, cell index, cell center, cell neighbours, cell volume, cell surface area. e.g. [{‘name’=6,’ index’=6 , ’center’=[0,1,1], ’neighbors’=[2,3,4,5], ‘volume’=462, ‘surface’=64}]. But be aware, I stored three additional information (trianglesArea, tissues, trianglesInfo) in the cell1’s properties here. These three properties should be individual.
global_current_assignments: current assignment, an array of integers. E.g.[0,0,8,8,0,0,0]
global_pair_possibilities: pair (sisters) name and possibility, an array of objects, e.g.[{‘pairs’:[1,2], ‘possibility’:0.63}, {‘pairs’:[3,4], ‘possibility’:0.28}];
 */

// mongoose is already set up in app.js
// and available here as some kind of singleton
const mongoose = require('mongoose');

//TODO: add the sessionID
const globalSchema = new mongoose.Schema({
    // it would be nice to make it a unique 'key' on the database
    // side
    sessionId: String,
    // for us number of the cells are their name
    leftCells: [Number],
    currentAssignments: [Number],
    basicCells: [{
        name: String,
        index: Number,
        center: [Number],
        neighbors: [Number],
        volume: Number,
        surfaceArea: Number
    }],
    pairPossibilities: [{
        pairs: [Number],
        possibility: Number
    }],
    supportingCells: [Number],
    predictionConstrains: [[Number]]
}, {
    // let mongoose schema handle createdAt and updatedAt
    timestamps: true
});

// Compile mode from schema and export it
module.exports = mongoose.model('globalModel', globalSchema);