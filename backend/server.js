const express = require('express');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const cors = require('cors');
const menuRouter = require('./routes/menu');
const dataRouter = require('./routes/data');
const dataRWRouter = require('./routes/dataRW');
const jobRouter = require('./routes/job');
const globalRouter = require('./routes/global');

require('dotenv').config();

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// -------------- DATABASE ----------------
const DATABASE_URL = process.env.BACKEND_DATABASE_URL

const mongooseOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // Keep trying to send operations for 5 seconds
    serverSelectionTimeoutMS: 5000
}

// we are in 'top level' and can't use async/await, so
// we use old promise style .then and .catch
mongoose.connect(DATABASE_URL, mongooseOptions)
.then(() => {
    // The `mongoose.connect()` promise resolves to mongoose instance
    console.log('Connected to MongoDB');

    const BACKEND_ADDRESS = process.env.BACKEND_ADDRESS || 'localhost';
    const BACKEND_PORT = process.env.BACKEND_PORT || 3000;
    const BACKEND_URL = `http://${BACKEND_ADDRESS}:${BACKEND_PORT}`;
    const FRONTEND_URL = process.env.BACKEND_FRONTEND_URL || '*';
    const SESSION_SECRET = process.env.BACKEND_SESSION_KEY;
    
    // prepare the store for express-session
    const mongoStore = MongoStore.create({
        // reuse mongoose connection for session storage
        client: mongoose.connection.getClient(),
        collection: 'sessions'
    });

    // express-session middleware creation
    app.use(session({
        secret: SESSION_SECRET,
        resave: false,
        saveUninitialized: true,
        store: mongoStore,
        cookie: {
            // Equals 1 day (1 day * 24 hr/1 day * 60 min/1 hr * 60 sec/1 min * 1000 ms / 1 sec)
            maxAge: 1000 * 60 * 60 * 24
        }
    }));

    app.use(cors({
        // Access-Control-Allow-Origin
        // needed to fetch from a frontend
        // regexp or function can also be used
        origin: FRONTEND_URL,
        // Access-Control-Allow-Credentials
        // needed to send cookies from the frontend
        credentials: true,
    }));

    // -------------- ROUTES ----------------
    // frontend part
    app.use('/', express.static('frontend', { 'index': 'html/index.html'}));
    // backend part
    app.use('/data', dataRouter);
    app.use('/dataRW', dataRWRouter);
    app.use('/menu', menuRouter);
    app.use('/job', jobRouter);
    app.use('/global', globalRouter);

    app.listen(BACKEND_PORT, BACKEND_ADDRESS, () => {
        console.log(`Example app listening at ${BACKEND_URL}`);
    });

}).catch((e) => {
    console.log(e);
    // The `mongoose.connect()` promise rejects if initial connection fails
    console.log('MongoDB connection failed');
});

